#include<stdio.h>
#include<setjmp.h>

jmp_buf buf;

// warning: type specifier missing, defaults to 'int' [-Wimplicit-int]
banana()
{
    printf("in banana() \n");
    longjmp(buf,1);
    //一下代码不会呗执行
    printf("you'll never see this,because i longjmp'd");
}

main()
{
    if(setjmp(buf))
    {
        printf("back in main\n");
    }
    else
    {
        printf("first time through\n");
        banana();
    }
}