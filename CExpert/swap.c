#include<stdio.h>
int main()
{
    int num = 3;
    int *a =&num,*b =&num;
    //如果ab指向重叠的对象，算法失败
    //如果变量存储与寄存器中，算法失败
    *a ^= *b;
    *b ^= *a;
    *a ^= *b;
}

// a=b-a;
// b=b-a;
// a=b+a;