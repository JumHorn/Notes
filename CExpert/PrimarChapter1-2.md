# 第一章 开始
***
### 细节语法

1. 注释不能嵌套

```C++
int main()
{
    /* 注释/*不能*/嵌套*/
    // 注释 * /之间不能有空格
    int i=0;
    int j=1;
    cout<<i+j<<endl;
}  
```

2. cin结束符

```C++
int main()
{
    int n;
    while(cin>>n)
    {
        cout<<n<<",";
    }
    cout<<endl;
    return 0;
} 
```

3. 键盘输入文件结束符

windows是 ctrl+z \
linux/unix 是 ctrl+D

4. 文件重定向

< infile > outfile,命令行处理

# 第二章 变量和基本类型

1. 无符号数

```C++
#include<iostream>
using namespace std;
int main()
{
    unsigned n;
    cout<<sizeof(n)<<endl;
    for(n=10;n>=0;--n)
    {
        cout<<n<<" ";
    }
}  
```
unsigned和unsigned int 一样，用无符号数，上面的循环是死循环  \
进行数值计算时优先转化为无符号数

2. 字面值常量

以0开始的整数是8进制  \
以0x开始的整数是16进制

3. 转义序列

\后面跟随的数是八进制，八进制数不能超过三个  \
\x后面的数是16进制  \
eg：\7 (响铃)  \12 (换行符)  \40 (空格)  \
\0 (空字符)  \115 (字符M)  \x4d (字符M)

4. 字面值类型

L'a'         //宽字节wchar_t     \
u8"hi"       //utf-8字符串         
42UUL        //unsigned long long 整数  \
1E-3F        //float浮点数        \
3.14159L     //long double浮点数

5. 指针的引用

```C++
int main()
{
    int _=9;
    int *p = &_;
    int *&r=p;
    cout<<*r<<endl;
}  
```
指针的引用可以解决指针作为参数时，改变指针指向的操作无效
```C++
TreeNode* sortedArrayToBST(vector<int>& nums) 
{
    if(nums.size()==0)
    {
        return NULL;
    }
    TreeNode* root;
    Array2BST(root,nums,0,nums.size()-1);
    return root;
}
//void Array2BST(TreeNode* root,vector<int>& nums,int L,int R)
//不加引用指针是始终返回空，因为指针作为参数，不可以改变指针指向
void Array2BST(TreeNode* &root,vector<int>& nums,int L,int R)
{
    if(L<=R)
    {
        int middle = (L+R)/2;
        root = new TreeNode(nums[middle]);
        Array2BST(root->left,nums,L,middle-1);
        Array2BST(root->right,nums,middle+1,R);
    }
}
```

6. const常量引用

```C++
int main()
{
    const int _;  //没有初始化
    int *p = &_;  //不能从const int * 转为 int *
    int *&r=p;
    cout<<*r<<endl;
}  
```
const默认在本文件内有效，要想在多个文件中共享const变量，需要在const的声明和定义前都加上extern \
常量引用初始化 \
```C++
double dev = 3.14;
const int &r = dev;
//此时相当于
double temp;
temp = dev;
const int &r=temp;
//这里用指针是改变不了原来的dev的，所以C++非常量不许这种引用
```

7. 