#include<stdio.h>

void print()
{
    printf("hello world\n");
}

int main()
{
    void (*p)();
    p = print;
    p();
    (*p)();
    //声明和使用相似的设计哲学
    (*******p)();
}