#include<stdio.h>

int main()  
{
    const int two = 2; //现在编译器不会出现错误
    //case条件不能是变量，只能是整形常量包括char, short, int, long
    int i=2;
    //一条switch语句最多257个case标签
    //8bit的所有情况加上EOF
    switch(i)
    {
        //没有break则顺序执行，不执行default
        case 1:printf("case 1\n");
        //default 打错了也能编译通过
        defult:printf("default"); //default 顺序不影响
        case two:printf("case 2\n");
        case 3:printf("case 3\n");
    }
}