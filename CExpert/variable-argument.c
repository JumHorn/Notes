#include<stdio.h>
#include<stdarg.h>
//需要用到这些定义
// void va_start( va_list arg_ptr, prev_param );
// type va_arg( va_list arg_ptr, type );
// void va_end( va_list arg_ptr );
int sum(int i,...)
{
    int j;
    va_list arg_ptr;  //指向参数的指针
    va_start(arg_ptr,i);
    //然后用va_start宏初始化变量arg_ptr,这里的第二个参数是三个点前面的参数
    //暂时理解为可变参数定位,并且是可变参数的第一个参数
    j=va_arg(arg_ptr, int);
    printf("%d,%d,",i,j);
    j=va_arg(arg_ptr, int);
    printf("%d",j);
    //然后用va_arg返回可变的参数,并赋值给整数j. va_arg的第二个
    //参数是你要返回的参数的类型,这里是int型
    va_end(arg_ptr); 
    //清理列表
    return j;
}

int main()
{
    sum(6,2,3,4);
}
