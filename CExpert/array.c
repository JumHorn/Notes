#include<stdio.h>

void arr5()
{
    int arr[10];
    arr[5] = 8;
    //编译器都是翻译为5+arr
    printf("%d\n",5[arr]);
}

char ga[3];

void cafun(char ca[])
{
    printf("ca:\n%#x,%#x,%#x\n",&ca,&(ca[0]),&(ca[1]));
    char ***p; //直接使用会发生segment fault
    //char ***p = (char***)malloc(sizeof(char));
    // p = &(&ca);
    // *p = &ga;
}
void pafun(char *pa)
{
    //就表示在输出时是以带0x前缀的十六进制
    printf("pa:\n%#x,%#x,%#x\n",&pa,&(pa[0]),&(pa[1]));
}


void gafun()
{
    printf("ga:\n%#x,%#x,%#x\n",&ga,&(ga[0]),&(ga[1]));
}

int main()
{
    char a[3];
    printf("%#x,%#x\n",a,&a);
    cafun(a);
    pafun(a);
    gafun();
}