1. memset初始化为1
```c++
#include <stdio.h>
#include <string.h>
int main() {
    int array[5];
    int a;
    while(~scanf("%d",&a)){
        memset(array,a,sizeof(array));
        printf("%d  %d\n",array[0],array[1]);
    }
    return 0;
}
```
***
memset是按字节赋值的，取变量a的后8位二进制进行赋值。

1的二进制是（00000000 00000000 00000000 00000001），取后8位（00000001），int型占4个字节，当初始化为1时，它把一个int的每个字节都设置为1，也就是0x01010101,二进制是00000001 00000001 00000001 00000001，十进制就是16843009。

之所以输入0,-1时正确，纯属巧合。

0，二进制是（00000000 00000000 00000000 00000000），取后8位（00000000），初始化后00000000 00000000 00000000 00000000结果是0
-1，负数在计算机中以补码存储，二进制是（11111111 11111111 11111111 11111111），取后8位（11111111），则是11111111 11111111 11111111 11111111结果也是-1
***

2. 补码

取反加1和减1取反是一样的

3. switch

switch括号内的书必须是整形，包括char, int, short, long
不包括double

4. 数值计算先强制类型转化

```C++
int main()
{
    int n=5;
    long temp = pow(10,n),maxnum=0,multi;  
    for(int i=temp-1;i>=0;i--)
    {
        for(int j=temp-1;j>=0;j--)
        {
            multi = (long)i*j;
            //multi = i*j; //结果不一样
            cout<<i<<","<<j<<","<<multi<<endl;return 0;
            if(multi>=9966006699)
            {
                cout<<i<<"*"<<j<<"="<<i*j<<endl;
            }
            else
            {
                break;
            }
        }
    }  
    return 0;    
}
```