#include<stdio.h>
int main()
{
    int x=2,y=3,temp;
    temp = y+++x;
    printf("%d\n",temp);
    // temp = y+++++x; //缺少空格
    // temp = y++ ++ +x; //不能编译通过
    printf("%d\n",temp);
}
//y+++x的汇编语句
// 4           int x=2,y=3,temp;
// => 0x0000000100000f3f <+15>:    movl   $0x2,-0x4(%rbp)
//    0x0000000100000f46 <+22>:    movl   $0x3,-0x8(%rbp)

// 5           temp = y+++x;
//    0x0000000100000f4d <+29>:    mov    -0x8(%rbp),%eax
//    0x0000000100000f50 <+32>:    mov    %eax,%ecx
//    0x0000000100000f52 <+34>:    add    $0x1,%ecx
// ---Type <return> to continue, or q <return> to quit---
//    0x0000000100000f55 <+37>:    mov    %ecx,-0x8(%rbp)
//    0x0000000100000f58 <+40>:    add    -0x4(%rbp),%eax
//    0x0000000100000f5b <+43>:    mov    %eax,-0xc(%rbp)