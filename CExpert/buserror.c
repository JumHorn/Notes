#include<stdio.h>

union 
{
    char a[10];
    int i;
}u;
//现在的编译器不会引起错误
int main()
{
    int *p = (int *)&(u.a[1]);
    *p = 65; /* p中未对齐的地址会引起一个总线错误！ */
    printf("%c\n",u.a[1]);
}