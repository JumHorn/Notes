# STL

**user defined comp must meets the requirement comp(a,b)=true then must comp(b,a)=false**
or heap overflow may occur

# std/boost thread
```C++
#include<thread>
#include<iostream>
using namespace std;

void run()
{
	for(int i=0;i<5;i++)
		cout<<"hello world"<<endl;
}

int main()
{
	//thread start automaticly when ctor finished
	//if join/detach not been called before dtor, std::terminated will be called
	//this cause program abort error
	thread t(run);
	t.detach();
	cout<<"current thread"<<endl;
	return 0;
}
```

# pointer
指针是否想等，是判断是否指向同一地址
```C++
#include<iostream>
using namespace std;

//是否指向同一地址
bool isEqual(int*& p1,int* p2)
{
    if(p1==p2)
        cout<<"equal"<<endl;
    else
        cout<<"not equal"<<endl;
}

int main()
{
    int a=3;
    int *p1=&a;
    int *p2=&a;
    if(p1==p2)//equal
        cout<<"equal"<<endl;
    isEqual(p1,p2);//equal
    char* p3=(char*)&a;
    if(p1==p3)//编译错误
        cout<<"equal"<<endl;
    return 0;
}
```

# union的类型
union内部只能包含POD类型
不可以包含任何自定义的construct和copy等
所以要想通过编译器编译要添加命令-std=c++11

# enum类型重定义
```C++
enum T0
{
	t0,
	t1
};

enum T1
{
	t0,
	t1
};
```
这两处的t0和t1重定义

# 转换函数
1. QByteArray自动转换为char*,是由于在源码中实现了一下转换函数，所有Qt源码都可以在头文件中找到，便于学习
```C++
operator const char*() const;
operator const void*() const;
```
2. explicit修饰转换函数，防止隐式类型转化
```C++
explicit operator const char*() const;
```

# Linux GNU bash version 4.4.23

sftp://192.168.10.43在文件查看器下打开，即可复制文件linux下
Windows下bash打开
sftp://user@192.168.10.43后面和ftp命令一致
get -r folder递归复制文件夹

curl commandline URL
curl http://ip:port/filename -O采用服务器的文件名
curl ftp://ip/filename -u "user:password"

wget 获取ftp目录
wget -r ftp://user@ip/filepath --ftp-password 1

计算代码行数
find . -name *.cpp | grep -v “moc_.*.cpp” | xargs wc -l

vim按下ctrl+s，停止向终端输入，出现假死状态，可以按ctrl+q退出这种状态

修改rpath
chrpath将编译好的二进制文件的rpath修改

shell字符串分割
#!/bin/bash
string="hello,shell,split,test"
array=(${string//,/ })
for var in ${array[@]}
do
   echo $var
done


shell打印正在执行的命令
set -v
或者
#!/bin/bash -v

运算符
-eq -ne -gt -lt -ge -le

获取上一条命令返回状态$?
if [$? ne 0]如果命令执行成功
获取命令执行输出
var=$(command)或者var=`command`
获取变量内容
${var}或者$var

mv隐藏文件
mv .[^.]* path
表示以.开始，加上不是.的任意字符，再加其它字符，避免匹配.和..

Linux host文件修改/etc/hosts
Windows host文件修改，类似dns
Windows/system32/drivers/etc/hosts

查看编码
file filename

编码转换为utf8bom
echo -ne '\xEF\xBB\xBF' > filenames
追加bom将文件改为utf8bom格式
sed -i  “1 \xEF\xBB\xBF” filename在文件第一行插入二进制bom
sed -i 's/[ \t]*$//g'  filename删除行尾空格和tab

编码转换命令
iconv -l列出所有编码
iconv -f GB2312 -t UTF-8 abc.txt -o abc.txt
转换编码

查找文本
grep -rn "中文乱码" ./ --include=*·cpp
grep -rn "乱码" ./ --include=*·cpp --exclude=moc_*.cpp
grep -rn "乱码" ./ --include=*{.cpp,.h}
正则表达式搜索
-o 表示only match
.表示任意字符
*表示重复
-C 5显示上下5行
grep -o "(.*)" a.cpp
grep -o "\".*\"" a.cpp
显示不匹配的行
grep -v "not match"

sed文本替换
sed -i “s/oldstr/newstr/g”
sed -i "s/^/head/"每行前面加上head
sed -i "1 s/^/head/"第一行行前面加上head
sed -i  “1i \xEF\xBB\xBF” 第一行插入字符，有换行
sed的正则表达式和grep是一样的
正则表达式匹配的部分用&表示
sed -i “s/u8\“.*\“/QString(&)/g“
sed -i 's/[ \t]*$//g'  filename删除行尾空格和tab


关于软链接和硬链接
硬链接在删除文件时，如果还有其他硬链接指向该文件，则不删除文件的inode，依然存储在硬盘上

Linux软连接源必须是全路径
如果使用相对路径则应该是以软连接所在路径为基础

scp拷贝软链接，拷贝了实际文件
tar压缩包后拷贝正常

cp命令与scp处理软链接类似
cp -d 只拷链接
mv命令不受影响

关于cp权限变化问题
cp -a 则不会改变权限，也只拷贝软链接
cp --parents 在cp时同时创建文件夹


配置环境变量
export PATH=$PATH:/usr/local
设置永久生效/etc/profile所有用户
.bashrc当前用户生效
set设置环境变量
unset取消环境变量

telnet远程登录
telnet ip即可
输入用户名密码，执行Linux命令和ssh类似

rename正则表达式修改多个文件名称
批量添加TXT后缀
rename 's/$/\.txt/'  *
批量删除TXT后缀
rename 's/\.txt$//' *

dmesg查看系统日志
系统日志/var/log/syslog
内核日志/var/log/kern.log

find排除权限不足文件
find / -name git 2>/dev/null
find路径必须在表达式之前
是find的文件加引号
find排除目录.git
find . -not -path "**/.git/**"
find按照时间查找文件 find . -type f -mtime -2两天前修改过 find . -type f -ctime -3三天内修改过

xargs配合命令使用
递归删除指定类型文件
find ./ -name *.bak | xargs rm -f
复制指定类型文件
find ./ -name *.cpp | xargs -I{} cp {} dstdir


在当前shell下执行脚本
source filename
相当于. filename之间有空格

远程登录时执行/etc/profile
本地用户登录时执行～/.bashrc或.bash_profile

Linux ssh shell颜色
.bashrc所有bash的配置
.profile主要调用bashrc
.bash_profile用户自定义配置
我删除用户自定义配置后bash颜色恢复正常

远程启动带界面程序
oli@bert:~$ ssh tim
oli@tim:~$ export DISPLAY=:0
oli@tim:~$ firefox
root用户需要多加一条命令
xhost inet:root@ip

shell 重定向
步骤1
ps 看到设备名
pts/0 bash
pts/0 ps
步骤2
这些设备都在dev/pts下
echo string > /dev/pts/1
特殊情况
qDebug输出在stderr上
所以使用
./program 2 > /dev/pts/0
例子
./program 2>/dev/pts/0 1> /dev/pts/0
0表示标准输入
&>将1和2同时重定向，程序无输出

后台进程
command & 命令行窗口退出后程序结束

nohup command &
程序退出后结束，用户异常退出也会结束

setsid command &另起session跑程序
将所有命令放在()中跑，可以在subshell中跑

查看系统字符集
locale | grep LANG

&& ||命令
command1 && command2
表示command1执行成功后再执行command2
||表示command1执行失败后再执行command2

切回到前一次路径
cd -
查看进程当前路径
pwdx pid

生成文件md5
md5sum -b filename
相关命令sha256sum，sha512sum

设置系统时间
hwclock --set --date ‘08/02/2012 12:00:00’
hwclock -w将系统时间设置为硬件时间
如果硬件时间不对
date -s设置的时间在重启后失效


# network

ip addr添加和删除ip
ip addr add 192.168.1.3/24 dev eth0
ip addr del 192.168.1.3/24 dev eth0

ifconfig修改ip
ifconfig eth0 192.168.1.3/24 up
ifconfig eth0 192.168.1.3/24 down

ipconfig修改mac地址
ifconfig eth0 hw ether 10:81:84:00:10:06

tcpdump
-D 显示所有interfaces
-i 指定interfaces
-n显示IP地址，而不是dns
-v 显示protocol
-A 以ASCII形式显示内容
src IP
dst IP
例子
tcpdump -ni eth0 udp port 1414

广播不绑定IP无法发送Linux
1. 广播地址255.255.255.255改为局域网内的广播192.168.2.255
2. 通过广播路由表解决，iptable命令



# compiler

gcc支持变长数组，在栈上开辟，该变长数组只能在栈上，不能位于数据段，即不能是全局变量和静态变量

gcc内存检查
-fsanitize=address
int越界检查
-fsantize=undefined

gdb program core.dump
bt查看调用堆栈backtrace
查找coredump文件
cat /proc/sys/kernel/core_pattern
core表示当前目录

gdb条件断点
break 7 if i==99
查看断点，info还能查看很多东西，执行info有提示
info breakpoints
打印可以查看所有断点
print打印对象可以调用对象成员函数
print str.c_str()
删除断点
clear清除当前行断点
d删除所有断点
d 断点编号

linux ld命令有内部链接脚本
可以不用写-pthread
ld -verbose查看脚本内容


UE显示utf8文件自动加\0x00，在配置中unicode/utf-8检测关闭

gcc
inline函数不能分开定义，必须在同一编译单元，否则会出现undefined reference

VS编译时自动跳过一些项目
编译时选择”重新生成解决方案“，会出现，”已跳过生成: 项目”, 不编译, 也不报错
解决方法：查看“生成”-> “配置管理器”-> 设置要生成的项目，勾选生成项目即可

vs2015无法解析的外部符号_printf
附加依赖项添加lib
legacy_stdio_definitions.lib

gcc下内联函数在release模式下编译不通过，会出现未定义的引用错误

gcc -pipe加速编译，不生成中间文件，使用管道

g++生成汇编文件-S
g++ -S main.cpp -o main.cpp.s
g++生成预处理文件 -E 宏和头文件预处理
g++ -E main.cpp > main.cpp.i


VS显示编译命令
项目 属性 c/c++ 常规 取消显示启动版权标志 否
项目 属性 连接器 常规 取消显示启动版权标志 否

字符编码问题
gcc文件是utf8，则char*编码就是utf8，gcc的char*依赖文件格式
msvc的文件编码是utf8，char*的编码是系统默认，只能用u8来指定，或者加上#pragma execution_character_set(“utf-8“)作用一样

gcc编译时检查库的连接，不要等到运行时检查
-Wl,--no-undefined逗号前后没有空格


重复定义的符号
gcc链接动态库时不检查重定义符号，-ltest.so 哪个动态库在前，使用哪个符号
msvc也不检查重定义符号，优先选择附加库目录中靠前的lib

msvc默认头文件包含路径bug未知，链接重定义问题未知
include cpp文件，在工程中就不要再编译它


nm查看动态库的导出符号
相似命令objdump

VS没写导出不会生成lib文件

VS重置
devenv /resetuserdata /resetsettings

查看gcc内置宏
gcc -dM -E -</dev/null

修改文件时间为当前时间,可以用于重新编译
touch filename

make install指定路径
make prefix=/usr/local/ install

查看动态库依赖
可以看出哪些not found
ldd libname.so
可以看出rpath
readelf -d libname.so
可以看一切可打印字符串
strings libname.so

设置运行时动态库查找
1. 将路径编译到二进制文件中
-Wl,-rpath,.
可是命令行不在当前路径时执行任会出错
需要，注意引号
-Wl,-rpath,'$ORIGIN'
在makefile中需要用$$来表示一个$
在命令行中输入$ORIGIN也要单引号包含，否则需要\转义

2. 配置/etc/ld.so.conf
在/etc/ld.so.conf.d/*.conf写入系统路径
该方法需要ldconfig主动刷新系统缓存
3. 设置环境变量LD_LIBRARY_PATH
中标麒麟下发现该环境变量结尾多了个:号
相当于加了.环境变量，将当前路径加入到环境变量

查找顺序
rpath大于LD_LIBRARY_PATH大于/etc/ld.so.conf

Linux非标准库名不能用-l来链接
可以去掉-l直接加上库全名

头文件包含顺序出错
不同目录下有相同的头文件
gcc查找哪个头文件
头文件版本不一致，导致代码错误


# git

git合并其他分支的部分文件
git checkout --patch master src/*.cpp
git合并策略默认recursive按时间顺序合并
resolve策略，文件一一合并，不自动合并

git默认推送到和本地
git config push.default matching

git使用http协议下载可以使用代理
terminal下代理需要添加环境变量https_proxy

Windows下git bash启动sshd服务
先创建服务器密钥
#ssh-keygen -t dsa -f /etc/ssh/ssh_host_dsa_key
#ssh-keygen -t rsa -f /etc/ssh/ssh_host_rsa_key
然后启动服务
#/usr/bin/sshd
远程登录的密码为Windows系统的用户名和密码
启动失败解决方法
先在git安装目录下的/etc目录下创建passwd文件，内容为
sshd:x:74:74:Privilege-separated
SSH:/usr/bin/sshd:/sbin/nologin

git subcommand --help查看网页帮助
git subcommand --h在命令行查看帮助

允许他人直接push我的本地仓库
git config receive.denyCurrentBranch ignore

git合并错误 error: path ‘config/db.php’ is unmerged
$ git checkout  config/db.php
error: path 'config/db.php' is unmerged
$ git reset foo/bar.txt
$ git checkout foo/bar.txt

git没有pull的情况下commit，撤销上次提交
git reset HEAD~1 表示head前一次提交

git rebase删除提交信息
当我们一个分支上面有多个提交，且部分提交是为了弥补之前的提交所做的细微改动。这个时候可以使用fixup将这些细微的提交合并（被合并掉的提交的message将被删除），这样我们主分支的git log graph看起来就更加干净
依然无法推送到远程分支，可以先删除远程master分支，再将改好的分支重命名到master，可能用到git remote set-head

git切换到tag版本
git checkout tagname
git checkout v2.9.5
git拉取分支需要在本地先创建分支
git checkout -b linux origin/linux

git tag -n5显示5行tag信息

删除远程tag
git tag -d tagname删除tag版本
git push origin :refs/tags/tagName
远程tag与本地tag同步
先删除本地tag
git tag -l | xargs git tag -d #删除所有本地分支
然后git pull

git使用http协议每次都要输入密码
git config --global credential.helper store

git diff不用less用cat
git config --global core.pager cat

git 将某个文件的历史版本变成最新版本
git checkout commitid filename
然后修改该文件后git push
git revert是整个版本修改，不对单个文件修改，这里容易产生误解


# Qt

qss样式表设置qt控件的属性
qproperty-*name
name是designer里面属性的英文名
例子
qproperty-icon qproperty-toolTip qproperty-text  qproperty-checkable

qt项目出现问题，清理qt项目，删除user文件，删除makefile，后重新打开

undefined reference to `vtable for’
QT中,类要支持信号与槽机制,需要继承自QObject并在头文件开头添加Q_OBJECT宏. 
如果使用QtCreator创建类时,没有选择继承自QObject类或其子类,而在创建后手工修改继承自QObject并手工添加Q_OBJECT宏,则在编译时有可能会出现”undefined reference to `vtable for’…….”错误

qmake编译debug版本
qmake CONFIG+=debug filename.pro
pro文件名在当前目录下，可以省略

QMAKE_LFLAGS += -Wl，--no-undefined

Linux下qtcreator的默认配置文件在.config/qtproject目录下，如果默认配置出差，删除这个配置文件即可

qglwidget要包含在gl.h和glu.h之前

qmake添加VS的ico图标
RC_FILE += project_NAME.rc


qtchooser选择默认Qt版本
qtchooser -install qt5.9 /opt/Qt5.9.1/5.9.1/gcc_64/bin/qmake
方法1：export QT_SELECT=qt5.9
方法2：配置default.conf文件选择qmake默认qt版本，在qtchooser目录下，可以搜索 里面除了default.conf还包含4.conf和5.conf等
/usr/lib/aarch64-linux-gnu/qtchooser目录

Qt生成的ui*.h文件不能乱放，导致cpp文件包含旧的ui*.h文件，从而出错

QtCreator使用VS2015编译器
需要修改Qt安装目录下的
msvc2015/mkspecs/msvc-version.conf
中添加一行
QMAKE_MSC_VER = 1900

qmake环境变量
* VAR = foobar => Assign value to variable when qmake is run
* $$VAR => QMake variable's value at the time qmake is run
* $${VAR} => QMake variable's value at the time qmake is run (identical but enclosed to separate from surrounding text)
* $(VAR) => Contents of an Environment variable at the time Makefile (not qmake) is run
* $$(VAR) =>Contents of an Environment variable at the time qmake (not Makefile) is run

qt在VS中x86和x64没有同时存在，是qt VS tool的bug，可以把默认设为32或64位
一直以为是VS的bug，并重装了两次，可见问题定位的重要性

qt 默认utf8依然乱码
源码字符集 可以默认utf8
可执行文件字符集在编译器上设置
gcc默认utf8，所以忽略不用管
-fexec-charset=charset

msvc2015
#pragma execution_character_set("utf-8")
或者字符串前面加u8
如果已经加上了u8，再使用fromLocal8Bit，QString依然会乱码
相当于从u8转本地gb2312，在转为u8存入QString

qt4在Linux下要加上一句
QTextCodec::setCodecForCString(QTextCodec::codecForName("UTF-8");

qt VS tools
open qt project file .Pro
可以在VS中直接打开qt工程
而qt中的源码在example下，可以直接打开学习

VS下moc文件没有生成
系统找不到指定路径
在VS中移出该文件，再把文件重新加入到该工程
该问题的本质是vcxproj.user文件没有在打开工程时重新生成，以后版本控制时应该加上该文件，该文件包含调试路径设置

QMAKE_CXXFLAGS += -std=c++11
QMAKE_LFLAGS += -Wl,-rpath,\’\$\$ORIGIN\’
搜索QtCreator相关的问题关键词是QMake

Qt staticMetaObject无法链接的外部符号
头文件没有包含在工程内，导致moc编译器没有编译含有Q_OBJECT宏的头文件，没有生成对应的moc的cpp文件，所以无法解析外部符号