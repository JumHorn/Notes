#include<stdio.h>

double sqr(x){return x*x;}
#define sqr(x) x*x  //这两行不能互换位置，否则expanded from macro 

int main()
{
    printf("%d\n",sqr(3+3)); //这里使用宏，下面的使用函数的定义
    printf("%d\n",(int)(sqr)(3+3));//浮点数当整形打印数据不正确，可以强制类型转化
}