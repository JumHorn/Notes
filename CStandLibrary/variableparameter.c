/*
va_list用于存储可变参数的指针数组
va_start用于为va_list申请空间
va_arg用于将va_list中的参数强制类型转化为实际需要的参数
va_end在函数返回时使用，避免不必要的错误(虽然可以省略)

由于可变参数采用的是相对地址
所以要注意可变参数前面必须有参数
*/
#include<stdio.h>
#include<stdarg.h>

//para指定可变参数个数
void print(int param_num,...)
{
    //用于存储可变参数
    int paramter_array[10];
    int i;
    va_list ap;
    va_start(ap,param_num);
    for(i=0;i<param_num;i++)
    {
        paramter_array[i]=va_arg(ap,int);
    }
}