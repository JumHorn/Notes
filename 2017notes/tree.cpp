#include<iostream>
#include<sstream>
#include<string>
#include<map>
#include<vector>
using namespace std;

//C++定义树
class CTree
{
//Attributes
private:
    int sum;
    stringstream ss;
    map<int,int> treemap;
    vector<int> treevec;
public:
    int val;
    CTree *left;
    CTree *right;
//Operations
    CTree(int x);
    //创建树
    CTree* create();
    //遍历树
    void traversing(CTree* node);
    //转化为string
    string TreetoString(CTree* node);
    //求和
    bool EqualNumber(CTree* root,int k);
    //变大
    CTree* enlarge(CTree* root);
    //增大
    void Increase(CTree* root);
    //初始化
    void init();
    //转化为图
    void TreetoMap(CTree* root);
    void MaptoTree(CTree* root);
    //转化为数组
    void TreetoVector(CTree* root);
    //计算左子树与右子树的差
    int LeafDifference(CTree *node);
    //计算所有子叶节点只和
    int LeafSum(CTree* node);
    //删除区间外节点
    CTree* TrimTree(CTree* root, int L, int R);
//Implemention
};

CTree::CTree(int x):val(x),left(NULL),right(NULL)
{}

void CTree::init()
{
    sum=0;
    //清空ss缓存
    ss.str("");
    ss.clear();
}

CTree* CTree::create()
{
    CTree *tree = new CTree(1);
    tree->left=new CTree(2);
    tree->right= new CTree(3);
    tree->right->left = new CTree(4);
    tree->right->left->right = new CTree(5);
    return tree;
}


void CTree::traversing(CTree* node)
{
    if(node==NULL)
    {
        return ;
    }
    cout<<node->val<<endl;
    traversing(node->left);
    traversing(node->right);
}
//遍历从小到大
bool CTree::EqualNumber(CTree* root,int k)
{
    TreetoVector(root);
    for(int i=0;i<treevec.size()-1;i++)
    {
        for(int j=i+1;j<treevec.size();j++)
        {
            if(treevec[i]+treevec[j]>k)
            {
                break;
            }
            if(treevec[i]+treevec[j]==k)
            {
                return true;
            }
        }
    }
    return false;
}

void CTree::TreetoVector(CTree* root)
{
    if(!root)
    {
        return ;
    }
    TreetoVector(root->left);
    treevec.push_back(root->val);
    TreetoVector(root->right);
}

void CTree::Increase(CTree* root)
{
    //i没有初始化
    if(!root)return;
    Increase(root->right);
    root->val=(sum+=root->val);
    Increase(root->left);
}

CTree* CTree::enlarge(CTree* root)
{
    init();
    TreetoMap(root);
    // for(int i=treemap.size()-1;i>=0;i--)
    // {
    //     cout<<treemap[i]<<endl;
    // }
    for(int i=treemap.size()-1;i>0;i--)
    {
        treemap[i-1]+=treemap[i];
    }
    init();
    MaptoTree(root);
    return root;
}

void CTree::TreetoMap(CTree* root)
{
    if(!root)
    {
        return ;
    }
    TreetoMap(root->left);
    treemap[sum++]=root->val;
    TreetoMap(root->right);
}

void CTree::MaptoTree(CTree* root)
{
    if(!root)
    {
        return ;
    }
    MaptoTree(root->left);
    root->val=treemap[sum++];
    MaptoTree(root->right);
}

string CTree::TreetoString(CTree *node)
{
    if(!node)
    {
        return "";
    }
    ss<<node->val;
    if(node->left||node->right)
    {
        ss<<"(";
        TreetoString(node->left);
        ss<<")";
        if(node->right)
        {
            ss<<"(";
            TreetoString(node->right);
            ss<<")";
        }
    }
    return ss.str();
}

int CTree::LeafDifference(CTree *node)
{
    //TODO
    return 0;
}

int CTree::LeafSum(CTree* node)
{
    if(node==NULL)
    {
        return 0;
    }
    return LeafSum(node->left)+LeafSum(node->right)+node->val;
}

CTree* CTree::TrimTree(CTree* root, int L, int R)
{
    if(!root)
    {
        return NULL;
    }
    else
    {
        root->left = TrimTree(root->left,L,R);
        root->right =TrimTree(root->right,L,R);
        if(root->val<L)
        {
            return root->right;
        }
        else if(root->val>R)
        {
            return root->left;
        }
        else
        {
            return root;
        }
    }
}

int main()
{
    // CTree *tree =NULL;
    // tree=tree->create();
    CTree *tree = tree->create();
    cout<<(tree->TreetoString(tree))<<endl;
    // tree->init(); 
    // tree->Increase(tree);
    tree->traversing(tree);
    cout<<"Leaves:"<<tree->LeafSum(tree)<<endl;
    //cout<<tree->EqualNumber(tree,7)<<endl;
}


//C语言定义树

//二叉树
// struct tree
// {
//     int val;
//     tree *left;
//     tree *right;
//     tree(int x):val(x),left(NULL),right(NULL){}
// };

//创建树
// tree * create()
// {
//     tree *head = new tree(1);
//     head->left = new tree(2);
//     head->right = new tree(3);
//     head->right->left = new tree(4);
//     head->right->left->right = new tree(5);
//     return head;
// }

//遍历二叉树
// void traversing(tree *node)
// {
//     if(node==NULL)
//     {
//         return ;
//     }
//     cout<<node->val<<endl;
//     traversing(node->left);
//     traversing(node->right);  
// }

// int main()
// {
//     tree *head = create();
//     traversing(head);
// }