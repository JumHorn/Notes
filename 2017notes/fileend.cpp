#include<iostream>
#include<fstream>
using namespace std;

//读取文件结束符0x1A,即是SUB，不论是文本方式打开还是二进制打开都会导致read停止读取
int main()
{
    ifstream fin("E:\\50mm.raw",ios::binary);
    ifstream fin1("E:\\25mm.raw",ios::binary);
    FILE *fp = fopen("E:\\50mm.raw","rb");

    char content[2];
    char ch,ch0,ch1;
    int t=0;
    int filelen;
    fin.seekg(0,ios::end);//指针到文件尾
    filelen = fin.tellg();//获取文件长度
    fin.seekg(0,ios::beg);//指针到文件头
    cout<<filelen<<endl;

    //无法获取完整文件
    // while(fin.read(&ch0,1))  //用eof判断文件结束
    // {
    //     //fin.read(&ch0,1);
    //     fin1.read(&ch1,1);
    //     t++;
    //     if(ch0!=ch1)
    //     {
    //         break;
    //     }
    //     // cout<<content[0]<<":"<<content[1]<<endl;
    // }

    // while (fin.get(ch))
	// {
    //     t++;
    //     if(t==8266)
    //     {
    //         printf("%d\n",ch);
    //     }
    // }

    //能够读取完整
    fscanf(fp,"%c",&ch);  //要多加一个以确保能够判断文件尾
    while(!feof(fp))
    {
        t++;
        fscanf(fp,"%c",&ch);
        if(t==8266)
        {
            printf("%d\n",ch);
        }
    }
    
    //这样也不行
    // do
    // {
    //     fscanf(fp,"%c",&ch);
    //     t++;
    // }while(!feof(fp));

    fin.seekg(8266,ios::beg);
    fin.get(content[0]);
    cout<<t<<endl;
    printf("%d",content[0]);
}
