#include<iostream>
//#include<WinBase.h>   //不能使用winbase.h,而要使用windows.h
#include<windows.h>
#include<string>
using namespace std;

int main()
{
	//获取路径下的.ini配置文件的server下的serverport参数，参数不是整数，则转化为整数
	int a = GetPrivateProfileInt("SERVER","SERVERPORT",1,"D:\\PACS\\local\\2013-08-01-10-30-00-2_AVI.ini");
	cout<<a<<endl;

	//GetPrivateProfileString("NETSET", "NetHost", "", set, sizeof(set), "e:\\test.ini"); 
//DWORD GetPrivateProfileString(LPCTSTR lpAppName,LPCTSTR lpKeyName,LPCTSTR lpDefaut,LPSTR lpReturnedString,DWORD nSize,LPCTSTR lpFileName);
//分析：
//返回值 ：
//DWORD ---------接收缓冲区的大小（long类型）
//型参：
//LPCTSTR lpAppName ---------- INI文件中的一个字段名，这个字串不区分大小写。
//LPCTSTR lpKeyName ---------- lpAppName 下的一个键名，也就是里面具体的变量名，这个字串不区分大小写。
//LPCTSTR lpDefaut ----------------如果没有其前两个参数值，则将此值赋给变量。指定的条目没有找到时返回的默认值。可设为空（""）。
//LPSTR lpReturnedString -------接收INI文件中的值的CString对象，指定一个字串缓冲区，长度至少为nSize。
//DWORD nSize ---------------------指定装载到lpReturnedString缓冲区的最大字符数量
//LPCTSTR lpFileName -----------完整的INI文件路径名
	return 0;
}