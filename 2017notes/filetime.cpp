#include<afx.h>
#include<iostream>
#include <windows.h>
#include <stdio.h>
using namespace std;



CString DiffFileTime(CString filepath)
{
    SYSTEMTIME time;       
    CString path;  
    path=filepath;//文件路径  
    CStdioFile file;  
    file.Open(path,CFile::modeRead);  
    FILETIME file_time;  
    FILETIME locationtime;  
    
    GetFileTime(file.m_hFile,NULL,NULL,&file_time);//获得文件修改时间  
    // FileTimeToLocalFileTime(&file_time,&locationtime);//将文件时间转换为本地文件时间  
    // FileTimeToSystemTime(&locationtime,&time);//将文件时间转换为本地系统时间  
    CString strTime;  
    // strTime.Format("%4d%2d%2d%2d%2d",time.wYear,time.wMonth,time.wDay,time.wHour,time.wMinute);//利用CString的格式化语句格式化时间数据  
    strTime.Format("%I64d",file_time.dwHighDateTime);
    cout<<strTime<<endl;
    file.Close();  
    return strTime;
}



void test(CString filepath)
{
    SYSTEMTIME st, lt, ft;
    
    GetSystemTime(&st);
    GetLocalTime(&lt);

    CString path;
    path = filepath;
    FILETIME file_time, locationtime; 
    CStdioFile file;  
    file.Open(path,CFile::modeRead); 
    GetFileTime(file.m_hFile,NULL,NULL,&file_time);
    FileTimeToLocalFileTime(&file_time,&locationtime);
    FileTimeToSystemTime(&locationtime,&ft);
    
    printf("The system time is: %02d:%02d\n", st.wHour, st.wMinute);
    printf(" The local time is: %02d:%02d\n", lt.wHour, lt.wMinute);
    printf("  The file time is: %02d:%02d\n", ft.wHour, ft.wMinute);
    printf("difftime: %02d\n", lt.wMinute-ft.wMinute);
}

//to judge a file is overdue or not
//return -1,can not open the file
//return -2,the file is not overdue
//return 1,the file is overdue
int FileOverdue(CString filepath)
{
    CStdioFile file;  
    if(!file.Open(filepath,CFile::modeRead))
    {
        return -1;
    }

    SYSTEMTIME lt, ft;
    GetLocalTime(&lt);

    FILETIME file_time, locationtime;
    GetFileTime(file.m_hFile,NULL,NULL,&file_time);
    FileTimeToLocalFileTime(&file_time,&locationtime);
    FileTimeToSystemTime(&locationtime,&ft);
    if(lt.wYear!=ft.wYear||lt.wMonth!=ft.wMonth||lt.wDay!=ft.wDay||lt.wHour!=ft.wHour)
    {
        return 1;
    }
    if(lt.wMinute-ft.wMinute>1)
    {
        return 1;
    }
    return -2;
}

int main()
{
    test("E:\\new.raw");
    // cout<<CompareFileTime("E:\\new.raw")<<endl;
}