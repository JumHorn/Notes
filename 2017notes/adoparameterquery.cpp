#include<iostream>
#include<afx.h>
#import "C:/Program Files/Common Files/System/ado/msado15.dll" no_namespace rename("EOF","adoEOF")
using namespace std;

int main()
{
    CoInitialize(NULL);
    try
    {
        _ConnectionPtr pConn(__uuidof(Connection));
        _RecordsetPtr pRec(__uuidof(Recordset));
        _CommandPtr pCmd(__uuidof(Command));

        pConn->ConnectionString = "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=sa;Password=;Initial Catalog=Test";
        pConn->CursorLocation=adUseClient;  //设置游标属性
        pConn->Open("","","",adConnectUnspecified);
        pCmd->ActiveConnection=pConn;
        //参数化查询代码
        // CString strSQL="select ?,name from ?"; //表名不能用参数,字段名不能用参数
        CString strSQL="select ?,name from employee";
        pCmd->CommandText=_bstr_t(strSQL);

        _ParameterPtr pParam1;
        _ParameterPtr pParam2;

        //类型 adInteger,adVarChar,adBSTR
        pParam1 = pCmd->CreateParameter(_bstr_t(""),adChar,adParamInput,2,"id");
        //pParam2 = pCmd->CreateParameter(_bstr_t(""),adChar,adParamInput,8,"employee");
        pCmd->Parameters->Append(pParam1);
        // pCmd->Parameters->Append(pParam2);

        printf("%s\n",(LPCSTR)pCmd->CommandText);
        pRec=pCmd->Execute(NULL,NULL,adCmdText);

        while(!pRec->adoEOF)
        {
            CString str=LPSTR(_bstr_t(pRec->GetCollect("name")));
            cout<<str<<endl;
            pRec->MoveNext();
        }

        pRec->MoveFirst();//移到首条记录
        while(!pRec->adoEOF)
        {
            CString str=LPSTR(_bstr_t(pRec->GetCollect((long)1)));//从0起，第一个字段
            cout<<str<<endl;
            pRec->MoveNext();
        }
        
        //关闭与释放
        pRec->Close();
        pConn->Close();
        pRec.Release();
        pCmd.Release();
        pConn.Release();
    }
    catch(_com_error& e)
    {
        cout<<e.ErrorMessage()<<endl;
        cout<<e.Description()<<endl;
    }
    CoUninitialize();
}
