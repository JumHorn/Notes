#include<afx.h>
#include<iostream>
using namespace std;

int main()
{
    /*-------------------------------------CString Replace----------------------------------------------*/
    CString s7 = "abcdefg";
    s7.Replace("a","x");
    cout<<s7<<endl;  

    /*-------------------------------------CString FindOneOf----------------------------------------------*/
    CString s6 = "abcdefg";
    CString sfind = "xvc";
    cout<<s6.FindOneOf(sfind)<<endl;  //2

    /*-------------------------------------CString Left Right----------------------------------------------*/
    CString s5 = "abcdefg";
    CString sl,sr;
    sl = s5.Left(3);  
    cout<<"left: "<<sl<<endl;  //abc
    sr=s5.Right(4);
    cout<<"right: "<<sr<<endl;  //defg

    /*-------------------------------------CString Mid----------------------------------------------*/
    CString s3 = "abcdefg";
    CString s4;
    s4 = s3.Mid(1,3);  //Extracts a substring of length nCount characters from this CString object, starting at position nFirst (zero-based)
    cout<<s4<<endl;
    cout<<s3<<endl;

    /*---------------------------invalid charactor in a string--------------------------------------*/
    CString s1 = "你好 世界";
    CString s2;
    //不是全部通用
    //左括号和单引号之间加空格，防止出现无效的字符串
    s2="select * from PACSVIEW_EXAMINATIONREPORT where COURSESTATE>=0 and EXAMRECORDEXAMSORT IN ( '"  + s1;
    //s2=s1+"hello";
    cout<<s2<<endl;
}