#include<iostream>
#include<string>
#include<vector>

using namespace std;

void print(const vector<string> &vec)
{
	for(vector<string>::const_iterator iter=vec.begin();iter!=vec.end();iter++)
	{
		cout<<*iter<<endl;
	}
}
int main()
{
	string strs[]={"hello","vector","string","key"};
	vector<string> words(strs,strs+4);
	//print(words);
	for(vector<string>::iterator iter=words.begin();iter!=words.end();)
	{
		//cout<<*iter<<iter->length()<<endl;
		if(iter->size()<5)
		{
			//两种方法
			iter=words.erase(iter);  //这种通用
			//words.erase(iter++);   
			//这个写法据某牛人说，只适合参数入栈顺序是从右向左的方式，参数入栈顺序是和具体编译器实现相关的。也就是，如果不幸遇到参数入栈顺序是从左向右的，上面的写法就不行了。
		}
		else
		{
			iter++;
		}
	}
	print(words);
	return 0;
}
