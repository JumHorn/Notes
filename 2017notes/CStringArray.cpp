#include<afx.h>
#include<stdio.h>
using namespace std;

//多线程操作数组会出现问题
//异常崩溃

#define MAXARRAYSIZE 50

DWORD WINAPI AddArray(LPVOID lParam) 
{
    CStringArray * temp = (CStringArray *)lParam;
    char array[20];
    char *p = array;
    for(int i=0;i<MAXARRAYSIZE;i++)
    {
        memset(array,'\0',sizeof(array));
        sprintf(array,"%d",i);
        temp->Add(p);
        if(i%17==0)  
            Sleep(1);
    }
	return 0;
}

DWORD WINAPI ReduceArray(LPVOID lParam) 
{
    CStringArray * temp = (CStringArray *)lParam;
    Sleep(1);
    while(temp->GetSize())
    {
        int count=temp->GetSize();
        if(count>10) count=10;
        for(int i=0;i<count;i++)
        {
            printf("current array size: %d\n",temp->GetSize());
            if(temp->GetSize()>0)
            {
                printf("%s\n",temp->GetAt(0));
                temp->RemoveAt(0);
            }
        }
    }
	return 0;
}

int main()
{
    CStringArray m_strArray;
    HANDLE handle[2];
	handle[0] = CreateThread(NULL, 0, AddArray, (CStringArray *)&m_strArray, 0, NULL);
    handle[1] = CreateThread(NULL, 0, ReduceArray, (CStringArray *)&m_strArray, 0, NULL);
    WaitForMultipleObjects(2, handle, TRUE, INFINITE);
}