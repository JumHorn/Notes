#include<iostream>
#import<msxml6.dll> rename_namespace("MSXML")
using namespace MSXML;

int main()
{
    CoInitialize(NULL); // 初始化COM环境
    MSXML::IXMLDOMDocumentPtr pDocument;  //创建对象指针
    HRESULT hr = pDocument.CreateInstance(__uuidof(MSXML::DOMDocument60));
    if(FAILED(hr))
	{
			std::cout<<"Failed to create the XML class instance"<<std::endl;
			return 0;
    }
    pDocument->loadXML("<Parent></Parent>");   //初始化主节点

    MSXML::IXMLDOMProcessingInstructionPtr pXMLProcessingNode=pDocument->createProcessingInstruction("xml", "version=\"1.0\""); // 创建XML声明
    MSXML::IXMLDOMElementPtr pElement0 = pDocument->GetdocumentElement();  //创建RootElement
    _variant_t vtObject;
	vtObject.vt = VT_DISPATCH;
	vtObject.pdispVal = pElement0;
    pDocument->insertBefore(pXMLProcessingNode,vtObject);
    
    pElement0->setAttribute("Depth",_variant_t("0"));
    //Create the child elements and set the attributes	
    MSXML::IXMLDOMElementPtr pElement1 = pDocument->createElement("Child1");	//Create first child element
    pElement1->setAttribute("Depth","1");
    pElement1->Puttext("This is a child of Parent");	//Set the element value
    pElement1 = pElement0->appendChild(pElement1);

    MSXML::IXMLDOMElementPtr pElement2 = pDocument->createElement("Child2");
    pElement2->setAttribute("Depth", "1");
    pElement2 = pElement0->appendChild(pElement2);	//Child2 is a sibling of Child1

    MSXML::IXMLDOMElementPtr pElement3 = pDocument->createElement("Child3");
    pElement3->setAttribute("Depth", "2");
    pElement3 = pElement2->appendChild(pElement3);	//Child3 is a direct child of Child2

    
    MSXML::IXMLDOMElementPtr pElement4 = pDocument->createElement("Child4");
    pElement4->setAttribute("Depth", "3");
    pElement4->Puttext("This is a child of Child3");
    pElement4 = pElement3->appendChild(pElement4);	//Child4 is a direct child of Child3

    char* filename = "XMLDOM.xml";
    pDocument->save((_variant_t)filename);  // 储存XML Document
    return 0;
}