#include<iostream>
#include<sstream>
//#include<thread>
#include<string>
#include<windows.h>
using namespace std;

void greet(string msg)
{
    for(int i=0;i<5;i++)
    {
        cout<<msg<<endl;
    }
}

int main()
{
    //多线程实现文件分发
    // thread t1(greet,"thread1");
    // t1.detach();
    // thread t2(greet,"thread2");
    // t2.detach();

    cout<<"enter a file end with \\"<<endl;
	string filepath;
	getline(cin,filepath);

	for(int i=1;i<6;i++)
    {
        stringstream ss;
        ss<<i;
        string s=ss.str();
        
        string cmd;
        cmd="copy "+filepath+"* "+"D:\\PACS\\local"+s+"\\";
        cout<<cmd<<endl;
        //WinExec(cmd.c_str(),SW_NORMAL);  //会顺序执行接下来的代码，不管指令是否执行完成
        system(cmd.c_str());
    }
}