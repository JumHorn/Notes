#include<iostream>
using namespace std;

int main()
{
	int n1=10;
	int n2=20;

	int const *p1;
	const int *p2;
	int * const p3=&n1;

	p1=&n1;
	p1=&n2;
	//*p1=n1;  //p1不能给常量赋值，p1可以改变指向

	p2=&n1;
	p2=&n2;
	//*p2=n1;  //p2不能给常量赋值，p2可以改变指向

	//p3=&n1;    //p3不能给常量赋值
	//p3=&n2;
	*p3=n2;  

	return 0;
}