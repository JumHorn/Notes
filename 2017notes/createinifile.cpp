#include<iostream>
#include<afx.h>  //mfc类定义
#include<string>
#include<windows.h>
#include<cstdlib>
#include<direct.h>
using namespace std;

//查找一层下所有文件
void findfile(char* filepath)
{
	CFileFind finder;
	BOOL bFind = finder.FindFile(filepath);  //找到文件夹下的所有文件(*.*)
	//cout<<bFind<<endl;  //找到一个或多个多重定义的符号

	while(bFind)
	{
		bFind = finder.FindNextFile();
		if(finder.IsDots())      //默认目录有.和.. [4/8/2017 Jum]
			continue;
		if (finder.IsDirectory())
			continue;
		cout<<finder.GetFilePath()<<endl;  //获取文件路径
		cout<<finder.GetFileName()<<endl;  //获取文件名
		cout<<finder.GetFileTitle()<<endl;  //获取文件名不含后缀名
	}
}

//递归查找目录下的所有文件和文件夹
void findallfile(char* filepath)
{
	CFileFind finder;
	BOOL bFind = finder.FindFile(filepath);  //找到文件夹下的所有文件(*.*)
	//cout<<bFind<<endl;  //找到一个或多个多重定义的符号

	while(bFind)
	{
		bFind = finder.FindNextFile();
		if(finder.IsDots())      //默认目录有.和.. [4/8/2017 Jum]
			continue;
		if (finder.IsDirectory())
		{
			char filepathname[256];
			sprintf(filepathname,"%s\\*",finder.GetFilePath());
			// cout<<filepathname<<endl;
			findallfile(filepathname);
		}
		// else //这里加else表示不输出文件夹
		cout<<finder.GetFilePath()<<endl;  //获取文件路径
		//cout<<finder.GetFileName()<<endl;  //获取文件名
	}
}

//filename:文件路径名
//pathname:文件名
void writeini(CString filepath,CString filename,char * server="D:\\PACSSERVER\\")
{
	WritePrivateProfileString("SERVER","SERVERIP","127.0.0.1",filepath+filename+".ini");
	WritePrivateProfileString("SERVER","SERVERPORT","8490",filepath+filename+".ini");
	WritePrivateProfileString("PATH","FileName",filename,filepath+filename+".ini");
	WritePrivateProfileString("PATH","LocalImagePath",filepath,filepath+filename+".ini");
	WritePrivateProfileString("PATH","ServerImagePath",server,filepath+filename+".ini");
}

//filepath：文件夹
void createini(CString filepath)
{
	CFileFind finder;
	BOOL bFind = finder.FindFile(filepath+"*");  //找到文件夹下的所有文件(*.*)
	//cout<<bFind<<endl;  //找到一个或多个多重定义的符号

	while(bFind)
	{
		bFind = finder.FindNextFile();
		if(finder.IsDots())      //默认目录有.和.. [4/8/2017 Jum]
			continue;
		if (finder.IsDirectory())
			continue;
		cout<<finder.GetFilePath()<<endl;
		writeini(filepath,finder.GetFileName());
	}
}

void getinifile(CString filepath)
{
	createini(filepath);
	//system("move /Y E:\\常用软件\\*.ini E:\\常用软件\\配置文件\\");//文件夹必须存在
	char cmd[1023];
	sprintf(cmd,"move /Y %s*.ini %s配置文件\\",filepath,filepath);
	//cout<<cmd<<endl;
	mkdir(filepath+"配置文件");
	system(cmd);
}

int main()
{
	//findfile("E:\\常用软件\\*");
	//findallfile("E:\\常用软件\\*");
	//writeini();
	
	//createini("E:\\常用软件\\");

	//CopyFile("E:\\常用软件\\*", "E:\\常用软件\\配置文件\\*", FALSE); 
	//TRUE和FALSE表示是否覆盖，目标文件夹必须存在,不支持通配符
	//cout << _pgmptr << endl;  //获取程序当前所在目录,eg: E:\CClearning\createinifile.exe
	//system("move /Y E:\\常用软件\\*.ini E:\\常用软件\\配置文件\\");
	//WinExec("calc",SW_NORMAL);  //会顺序执行接下来的代码，不管指令是否执行完成
	//GetCurrentDirectory();

	cout<<"输入文件夹，以\\结尾"<<endl;
	string filepath;
	getline(cin,filepath);
	getinifile(filepath.c_str());
	return 0;
}