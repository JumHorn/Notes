#include<iostream>
using namespace std;

class outer
{
public:
	class inner0
	{
	public:
		inner0()
		{
			data=3;
			cout<<"inner0"<<endl;
			cout<<data<<endl;
		}
	};
private:
	class inner1
	{
	public:
		inner1()
		{
			data=3;
			cout<<"inner1"<<endl;
		}
	};

private:
	static int data;
};

//静态成员变量需要在这里定义(必须)
//如果在类内定义，那么不建立对象时，静态数据成员无法初始化
int outer::data = 0;

int main()
{
	outer A;
	//cout<<A.data<<endl;
	outer::inner0 B0;
	//outer::inner1 B1; //无法访问

	return 0;
}