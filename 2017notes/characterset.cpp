// ANSI操作函数以str开头，如strcpy()，strcat()，strlen()；
// Unicode操作函数以wcs开头，如wcscpy，wcscpy()，wcslen()；
// ANSI/Unicode互为兼容的操作函数以_tcs开头 _tcscpy(C运行期库)；
// ANSI/Unicode互为兼容的操作函数以lstr开头 lstrcpy(Windows函数)；
// 考虑ANSI和Unicode的兼容，需要使用以_tcs开头或lstr开头的通用字符串操作函数。
// 最后加上头文件 #include <tchar.h>

//宽字节定义
// 为了让编译器识别Unicode字符串，必须以在前面加一个“L”,定义宽字节类型方法如下：
// wchar_t c = `A' ; 
// wchar_t * p = L"Hello!" ; 

#include<iostream>
#include<tchar.h>
using namespace std;

//TCHAR通用与不同字符集,一下是他的定义
// #ifdef UNICODE
// typedef wchar_t TCHAR
// #else
// typedef char TCHAR
// #endif

//_T()宏定义
// #ifdef UNICODE
// #define _T(x) L##x 
// #else
// #define _T(x) x 
int main()
{
    TCHAR msg[256] = _T("qwerqw");
    char array[128];
    cout<<msg<<endl;
}