// 接着介绍一下C++中的三种初始化方式：
// zero-initialization,default-initialization,value-initialization

// POD是Plain old data的缩写，它是一个struct或者类，且不包含构造函数、析构函数以及虚函数。
// 维基百科给出了更加详细的解释：C++的POD类型或者是一个标量值，或者是一个POD类型的类。
// POD class没有用户定义的析构函数、拷贝构造函数和非静态的非POD类型的数据成员。
// 而且，POD class必须是一个aggregate，没有用户定义的构造函数，没有私有的或者保护的非静态数据，
// 没有基类或虚函数。它只是一些字段值的集合，没有使用任何封装以及多态特性。

#include <iostream>  
using namespace std;  
  
struct A { int m; }; // POD  
struct B { ~B(){}; int m; }; // non-POD, compiler generated default ctor  
struct C { C() : m() {}; ~C(){}; int m; }; // non-POD, default-initialising m  
  
int main()  
{  
    A *aObj1 = new A;  
    A *aObj2 = new A();  
    cout << aObj1->m << endl;  
    cout << aObj2->m << endl;  
  
    B *bObj1 = new B;  
    B *bObj2 = new B();  
    cout << bObj1->m << endl;  
    cout << bObj2->m << endl;  
  
    C *cObj1 = new C;  
    C *cObj2 = new C();  
    cout << cObj1->m << endl;  
    cout << cObj2->m << endl;  
  
    delete aObj1;  
    delete aObj2;  
    delete bObj1;  
    delete bObj2;  
    delete cObj1;  
    delete cObj2;  
  
    return 0;  
} 

// 在所有C++版本中，只有当A是POD类型的时候，new A和new A()才会有区别。而且，C++98和C++03会有区别
// new A：不确定的值
// new A()：value-initialize A，由于是POD类型所以是zero initialization
// new B：默认构造（B::m未被初始化）
// new B()：value-initialize B，zero-initialize所有字段，因为使用的默认构造函数
// new C：default-initialize C，调用默认构造函数
// new C()：value-initialize C，调用默认构造函数