#include<iostream>
#include<vector>
using namespace std;

class Cat
{
public:
    Cat(int x = 0){}

    // Cat(){}
    // Cat(int x){}
};

class Dog
{
private:
    //不该调用带参构造函数，想当于在这里初始化int a = 5
    // vector<int> arr(10); //语法错误:“常量”
    Cat c(5); //语法错误:“常量”
    Cat c0;
};
