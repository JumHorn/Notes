#include<iostream>
#include<vector>
#include<algorithm>
#include<string>

using namespace std;

//void print(vector<int> &vec)
//{
//	for(vector<int>::iterator iter=vec.begin();iter!=vec.end();iter++)
//	{
//		cout<<*iter<<endl;
//	}
//}

//使用const修饰vector，则迭代器不能更改vec，所以迭代器指针也要改成const指针
void print(const vector<int> &vec)
{
	for(vector<int>::const_iterator iter=vec.begin();iter!=vec.end();iter++)
	{
		cout<<*iter<<endl;
	}
}

int main()
{
	int sum=0;
	vector<int> arr;
	arr.push_back(1);
	arr.push_back(4);
	arr.push_back(2);
	arr.push_back(3);
	print(arr);

	//升序排序
	cout<<"--------------------ASC------------------"<<endl;
	sort(arr.begin(),arr.end());
	print(arr);
	
	cout<<"--------------------SUM------------------"<<endl;
	for(int i=0;i<arr.size();i+=2)  //length不是vec的成员
	{
		sum+=arr[i];
	}
	cout<<sum<<endl;

	cout<<"--------------------STRING------------------"<<endl;
	//用string既有length也有size，两者一样
	string str;
	cout<<str.size()<<endl;
	cout<<str.length()<<endl;

	//反转
	cout<<"--------------------reverse------------------"<<endl;
	reverse(arr.begin(),arr.end());
	print(arr);

	//查找
	cout<<"--------------------find------------------"<<endl;
	vector<int>::iterator iter = find(arr.begin(),arr.end(),6);
	if(iter==arr.end())
	{
		cout<<"not find"<<endl;
	}
	cout<<*iter<<endl;  //竟然返回最后一个数	
	cout<<*arr.end()<<endl;

	//距离
	cout<<"--------------------distance------------------"<<endl;
	cout<<iter-arr.begin()<<endl;
	cout<<distance(arr.begin(),iter);
	
}
