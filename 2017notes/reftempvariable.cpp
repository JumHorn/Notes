#include<iostream>
#include<string>
using namespace std;

void printstr(string& str)
{
    cout<<str<<endl;
}

void f(int& a)
{
    cout<<a<<endl;
}

int main()
{
    printstr(string("HelloWorld!"));
    int a=1,b=3;
    f(a+b);  //a+b也是临时变量
    return 0;
}