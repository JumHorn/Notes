#include<iostream>
#include<windows.h>
using namespace std;

void create_process(char* process);
int main()
{
    //WinExec是一种函数，该函数只提供对16位机子的兼容。应用程序应该使用CreateProcess函数。
    //WinExec("C:\\Program Files\\Tencent\\TIM\\Bin\\QQScLauncher.exe",0);
    create_process("C:\\Program Files\\Tencent\\TIM\\Bin\\QQScLauncher.exe");
}

void create_process(char* process)
{
    STARTUPINFO si;
    PROCESS_INFORMATION pi;

    ZeroMemory( &si, sizeof(si) );
    si.cb = sizeof(si);
    ZeroMemory( &pi, sizeof(pi) );

    // Start the child process. 
    if( !CreateProcess( NULL,   // No module name (use command line)
        process,        // Command line
        NULL,           // Process handle not inheritable
        NULL,           // Thread handle not inheritable
        FALSE,          // Set handle inheritance to FALSE
        0,              // No creation flags
        NULL,           // Use parent's environment block
        NULL,           // Use parent's starting directory 
        &si,            // Pointer to STARTUPINFO structure
        &pi )           // Pointer to PROCESS_INFORMATION structure
    ) 
    {
        printf( "CreateProcess failed (%d).\n", GetLastError() );
        return;
    }

    // Wait until child process exits.
    WaitForSingleObject( pi.hProcess, INFINITE );

    // Close process and thread handles. 
    CloseHandle( pi.hProcess );
    CloseHandle( pi.hThread );
}

