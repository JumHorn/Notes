#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;

// print(vector<int>:: iterator iter)
void print(int dec)
{
    cout<<dec<<",";
}

bool order(int num1,int num2)
{
    if(num1<num2)
    {
        return false;
    }
    return true;
}

int main()
{
    int arr[] = {1,2,3,4,5};
    vector<int> vec(arr,arr+5);
    for_each(vec.begin(),vec.end(),print);
    sort(vec.begin(),vec.end(),order);
    cout<<endl;
    for_each(vec.begin(),vec.end(),print);
}