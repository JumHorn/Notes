#include<iostream>
#include<fstream>
#include<string>
using namespace std;

int main()
{
    // ios::out 为输出(写)而打开文件 
    // ios::ate 初始位置：文件尾 
    // ios::app 所有输出附加在文件末尾 
    // ios::trunc 如果文件已存在则先删除该文件 
    // ios::binary 二进制方式

    ifstream read;
    // read.open("E:\\安装清单.txt",ios::in|ios::out|ios::binary);
    // read.open("E:\\安装清单.txt",std::ios::binary);
    read.open("E:\\installcatgary.txt",ios::binary);
    // read.is_open()

    //二进制文件的读写要用到read和write
    char buff[256];
    int t;
    read.read(buff,sizeof(char)*256);
    cout<<buff<<endl;
    t=read.gcount();
    cout<<t<<endl;
    ofstream write("E:\\install.txt",ios::binary);
    write.write(buff,sizeof(char)*t);

    return 0;
}

//0x1A 文件结束
int FileEnd()
{
	ifstream sourcefile;
	sourcefile.open("E:\\BufferRAW.raw",ios::binary);
	// sourcefile.seekg(0, sourcefile.end);  
	// int srcSize = (int)sourcefile.tellg();  
	// cout<< srcSize <<endl;
	ofstream destfile;
	destfile.open("E:\\buff.txt");
	char ch;
	int totalbyte = 0;
	while (sourcefile.get(ch))
	{
		//打印到屏幕
		cout.width(2);
		cout.fill('0');
		cout << hex <<(unsigned int)ch << " ";

		//打印到文件
		destfile.width(2);
		destfile.fill('0');
		destfile << hex << (unsigned int)ch << " ";

		// if(totalbyte==7080)
		// {
		// 	cout<<endl;
		// 	cout << hex <<(unsigned int)ch << " ";
		// 	return 0;
		// }
		totalbyte++;
	}

	cout<<endl;
	cout<<"position"<<(int)sourcefile.tellg()<<endl;
	cout << endl;
	cout << dec << "totalbyte:" << totalbyte << endl;
	destfile.close();
}