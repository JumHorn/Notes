#include<iostream>
using namespace std;

class A
{
	float x,y; //默认private
public:
	A(float a=0,float b=0);
	A operator++();
	A operator++(int);
};

//A::A(float a=0,float b=0)  //重定义默认参数
A::A(float a,float b)
{
	x=a;
	y=b;
}

A A::operator++()
{
	A t;
	t.x++;
	t.y++;
	return t;
}

A A::operator++(int)
{
	A t;
	++t.x;
	++t.y;
	return t;
}

int main()
{
	A a(2,3),b;
	b=++a;
	b=a++;
	return 0;
}