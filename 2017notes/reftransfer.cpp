#include<iostream>
using namespace std;

class Dog
{
public:
    int height;  //c++不会初始化类的成员变量
    void print()
    {
        cout<<height<<endl;
    }
};

void objectparameter(Dog dog)
{
    dog.height=5;
}

void refobjectparameter(Dog &dog)
{
    dog.height=5;
}
int main()
{
    Dog d;
    objectparameter(d);
    d.print();
    refobjectparameter(d);
    d.print();
    return 0;
}