#include<iostream>
#include<string>
using namespace std;

//其他引用版本
void print(int*& arr)  //正确
{
    for(int i=0;i<5/* sizeof(arr)/sizeof(arr[0]) */;i++)
    {
        cout<<arr[i]<<",";
    }
    cout<<endl;
}

int main()
{
    int array[] ={1,2,3,4,5};
    string strs[]={"Jum","1023","Jum_1023"};
    int *p=array;
    print(p);
    // print(array);  //错误
    return 0;
}