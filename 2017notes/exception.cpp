#include<iostream>
#include<cstring>
using namespace std;

void base()
{
	try
	{
		int m,n;
		m=1;
		n=0;
		m=m/n;
		cout<<m<<endl;
	}
	catch(...)
	{
		cout<<"1/0"<<endl;
	}
}

bool isNumber(char * str) 
{
     if (str == NULL) 
        return false;
     int len = strlen(str);
     if (len == 0) 
        return false;
     bool isaNumber = false;
     char ch;
     for (int i = 0; i < len; i++) 
	 {
         if (i == 0 && (str[i] == '-' || str[i] == '+')) 
            continue;
         if (isdigit(str[i])) 
		 {
            isaNumber = true;
         } 
		 else 
		 {
           isaNumber = false;
           break;
         }
     }
     return isaNumber;
}

int parseNumber(char * str) throw(int) 
{
    if (!isNumber(str)) 
       throw int();
    return atoi(str);
}


int main()
{
	base();
	// char *str1 = "1", *str2 = NULL;
    // try 
	// {
    //     int num1 = parseNumber(str1);
    //     int num2 = parseNumber(str2);
    //     printf("sum is %d\n", num1 + num2);
    // }
	// //catch(int) //可以不生成对象
	// catch (int i) 
	// {
    //     printf("输入不是整数\n"); 
    //     //打印文件的路径,行号，str1,str2等信息足够自己去定位问题所在 
	// 	//cout<<i<<endl;
    // }
}