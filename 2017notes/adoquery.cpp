#include<iostream>
#include<afx.h>
#import "C:/Program Files/Common Files/System/ado/msado15.dll" no_namespace rename("EOF","adoEOF")
using namespace std;

int main()
{
    CoInitialize(NULL);
    try
    {
        _ConnectionPtr pConn(__uuidof(Connection));
        _RecordsetPtr pRec(__uuidof(Recordset));
        _CommandPtr pCmd(__uuidof(Command));

        pConn->ConnectionString = "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=sa;Password=;Initial Catalog=Test";
        pConn->Open("","","",adConnectUnspecified); 
        CString strSQL="select * from employee";
        pRec=pConn->Execute(_bstr_t(strSQL),NULL,adCmdText);
        while(!pRec->adoEOF)
        {
            CString str=LPSTR(_bstr_t(pRec->GetCollect("name")));
            cout<<str<<endl;
            pRec->MoveNext();
        }

        pRec->MoveFirst();//移到首条记录
        while(!pRec->adoEOF)
        {
            CString str=LPSTR(_bstr_t(pRec->GetCollect((long)1)));//从0起，第一个字段
            cout<<str<<endl;
            pRec->MoveNext();
        }

        printf("%d\n",pRec->Fields->Count);
        // cout<<(pRec->Fields->Count)<<endl;   //出现C库和C++库的冲突
        
        //关闭与释放
        pRec->Close();
        pConn->Close();
        pRec.Release();
        pCmd.Release();
        pConn.Release();
    }
    catch(_com_error& e)
    {
        cout<<e.ErrorMessage()<<endl;
        cout<<e.Description()<<endl;
    }
    CoUninitialize();
}