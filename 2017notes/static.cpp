#include<iostream>
#include<string>
using namespace std;

class A
{
public:
	static int m;
private:
	static int n;
};

//必须定义
int A::m=1;
int A::n=2;

class Cat
{
public:
    Cat(){cout<<"cat"<<endl;}
    int s;
};

class Dog
{
public:
    static Cat c;
    Dog(){cout<<"hello world"<<endl;}
    int weight;
    int height;
    Cat cat;
    operator string() const;
};

Dog::operator string() const
{
    return "hello world";
}

//必须定义
Cat Dog::c;

int main()
{
	A a;
	//A.m;  //不能用类名引用
	cout<<a.m<<endl;
	//可以用域引用
	cout<<A::m<<endl;

	Dog *d = new Dog;
    cout<<d->height<<endl;
    cout<<d->weight<<endl;
    cout<<((string)*d+" hehe")<<endl;
	cout<<((Dog::c).s)<<endl;
	
	return 0;
}