#include<iostream>
#include<string>
#include<algorithm>
using namespace std;

int main()
{
    string s = "MONDAY";
    transform(s.begin(),s.end(),s.begin(),::tolower);   //必须添加::域操作符,跳出std命名空间
    cout<<s<<endl;
    transform(s.begin(),s.end(),s.begin(),::toupper);
    cout<<s<<endl;

    char achar = 'a';
    achar = ::toupper(achar);
    cout<<achar<<endl;
}