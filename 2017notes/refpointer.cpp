#include<iostream>
using namespace std;

//引用版本
//参数为5个int的数组
// void print(int (&arr)[5])  //正确
// {
//     for(int i=0;i<5/* sizeof(arr)/sizeof(arr[0]) */;i++)
//     {
//         cout<<arr[i]<<",";
//     }
//     cout<<endl;
// }

//指针版本
// void print(int* arr)
// {
//     for(int i=0;i<5/* sizeof(arr)/sizeof(arr[0]) */;i++)
//     {
//         cout<<arr[i]<<",";
//     }
//     cout<<endl;
// }

//其他引用版本
void print(int*& arr)  //正确
{
    for(int i=0;i<5/* sizeof(arr)/sizeof(arr[0]) */;i++)
    {
        cout<<arr[i]<<",";
    }
    cout<<endl;
}

int main()
{
    int array[] ={1,2,3,4,5};
    int *p=array;
    print(p);
    // print(array);  //错误
    return 0;
}
