#include<iostream>
#include<string>
using namespace std;

class Dog
{
private:
    int numbers;
    void setnum(int n)
    {
        numbers=n;
        cout<<numbers<<endl;
    }
public:
    Dog()
    {}
    // explicit Dog(int n)   //不允许强制类型转化
    // Dog(int n)
    // {
    //     numbers=n;
    // }
    Dog(string str,int n)
    {
        numbers = n;
    }
    int getnum()
    {
        cout<<numbers<<endl;
        return numbers;
    }
};

int main()
{
    Dog d;
    *(int *)&d = 5555;
    // cout<<sizeof(d)<<endl;
    // Dog::Dog("a",6);
    //int (*pf)(void*);

    //通过函数指针调用
    // void (Dog::*pf)(int);
    // void *p;
    Dog *D = &d;
    // pf = &D->setnum;
    // (D->*pf)(78);

    //通过this指针调用
    int (*pf)(int,void *);
    pf = (int(*)(int,void*))&D->setnum;
    pf(778,D);
}