#include<iostream>
#include<sstream>
#include<cstdio>
#include<string>
#include<iomanip>
using namespace std;

int main()
{
	//使用c语言的sprintf
	// char str[256];
	char * s =new char[256];
	//format("%d",10);
	sprintf(s,"sprintf %d",10);
	cout<<s<<endl;
	cout<<sizeof(s)<<endl;
	delete[] s;

	stringstream str;
	str<<"a"<<1.34;
	cout<<str.str()<<endl;  //转化为字符串
	//右对齐，宽度为10个字符
	cout<<resetiosflags(ios::right)<<setw(10)<<str.str()<<endl; 

	//stringstream特性
	//>>遇到空格，\t,\n,\r,\b停止
	string line = "1 2\t3\n4\r5\b6";
	stringstream ss(line);
	string temp;
	while(ss>>temp)
	{
		cout<<temp<<endl;
	}

	// ss.str("");
	ss.clear();//没有这个下面失效
	//ss在第一次调用完operator<<和operator>>后，
	//来到了end-of-file的位置，此时stringstream会为其设置一个eofbit的标记位，
	//标记其为已经到达eof。查文档得知， 当stringstream设置了eofbit，
	//任何读取eof的操作都会失败，同时，会设置failbit的标记位，标记为失败状态。
	//所以后面的操作都失败了。
	try
	{
		ss<<"abc";
		cout<<ss.str()<<endl;
		cout<<"length:"<<ss.tellp()<<endl;//获取在输出序列中的位置（Input sequence）
	}
	catch(...)
	{
		cout<<"failed"<<endl;
	}
	return 0;
}