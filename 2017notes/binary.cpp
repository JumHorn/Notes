#include<iostream>
using namespace std;

int main()
{
	//字节
	cout<<sizeof(char)<<endl;
	cout<<sizeof(short)<<endl;
	cout<<sizeof(int)<<endl;
	cout<<sizeof(double)<<endl;

	char a=10; //00001010
	char b=12; //00001100
	
	cout<<short(a&b)<<endl;  //与
	cout<<short(a|b)<<endl;  //或
	cout<<short(~a)<<endl;   //非     获得负数，取反+1前面加负号
	cout<<short(a)<<endl;    //这些操作不改变a的值

	cout<<short(a^b)<<endl;  //异或
	cout<<short(!a^b)<<endl; //取反则是同或

	cout<<(a<<2)<<endl;      //<< 左移 用来将一个数的各二进制位全部左移N位，右补0，乘2的n次方
	cout<<(b>>2)<<endl;      //>> 右移 将一个数的各二进制位右移N位，移到右端的低位被舍弃，对于无符号数，高位补0，除以2的n次方
	cout<<short(a)<<endl;    //位移改变原来的值

	return 0;
}