#include<iostream>
#include<windows.h>
#include<winhttp.h>
#pragma comment(lib, "Winhttp.lib") 

int main()
{
    DWORD dwSize = 0;
    DWORD dwDownloaded = 0;
    LPSTR pszOutBuffer;
    BOOL  bResults = FALSE;
    HINTERNET  hSession = NULL, 
            hConnect = NULL,
            hRequest = NULL;

    // Use WinHttpOpen to obtain a session handle.
    //可以指定怎样使用代理（中间三个参数指定）
    // Use the WinHTTP functions asynchronously.（最后一个参数指定）
    hSession = WinHttpOpen( L"WinHTTP Example/1.0",  
                            WINHTTP_ACCESS_TYPE_DEFAULT_PROXY,
                            WINHTTP_NO_PROXY_NAME, 
                            WINHTTP_NO_PROXY_BYPASS, 0 );

    // Specify an HTTP server.
    //指定服务器名和端口，https：443
    if( hSession )
        hConnect = WinHttpConnect( hSession, L"www.microsoft.com",
                                    INTERNET_DEFAULT_HTTPS_PORT, 0 );

    // Create an HTTP request handle.
    //第二个参数指定请求类型，get，post，put
    //第三，五两个参数相关
    //第四个参数默认HTTP/1.1
    //获取文件类型
    if( hConnect )
        hRequest = WinHttpOpenRequest( hConnect, L"GET", NULL,
                                        NULL, WINHTTP_NO_REFERER, 
                                        WINHTTP_DEFAULT_ACCEPT_TYPES, 
                                        WINHTTP_FLAG_SECURE );

    // Send a request.
    // 第二个参数，指定header
    // 第三个参数，指定header的长度
    //An unsigned long integer value that contains the length, in characters, of the additional headers. 
    //If this parameter is -1L and pwszHeaders is not NULL, 
    //this function assumes that pwszHeaders is null-terminated, and the length is calculated
    // 第四个参数，request data
    //A pointer to a buffer that contains any optional data to send immediately after the request headers
    // data length
    // total length
    if( hRequest )
        bResults = WinHttpSendRequest( hRequest,
                                        WINHTTP_NO_ADDITIONAL_HEADERS, 0,
                                        WINHTTP_NO_REQUEST_DATA, 0, 
                                        0, 0 );


    // End the request.
    // 第二个参数This parameter is reserved and must be NULL.
    if( bResults )
        bResults = WinHttpReceiveResponse( hRequest, NULL );

    // Keep checking for data until there is nothing left.
    if( bResults )
    {
        do 
        {
            // Check for available data.the amount of data in bytes
            dwSize = 0;
            if( !WinHttpQueryDataAvailable( hRequest, &dwSize ) )
                printf( "Error %u in WinHttpQueryDataAvailable.\n",
                        GetLastError( ) );

            // Allocate space for the buffer.
            pszOutBuffer = new char[dwSize+1];
            if( !pszOutBuffer )
            {
                printf( "Out of memory\n" );
                dwSize=0;
            }
            else
            {
                // Read the data.
                ZeroMemory( pszOutBuffer, dwSize+1 );
                
                // BOOL WINAPI WinHttpReadData(
                //   _In_  HINTERNET hRequest,
                //   _Out_ LPVOID    lpBuffer,
                //   _In_  DWORD     dwNumberOfBytesToRead,
                //   _Out_ LPDWORD   lpdwNumberOfBytesRead
                // );                               
                if( !WinHttpReadData( hRequest, (LPVOID)pszOutBuffer, 
                                        dwSize, &dwDownloaded ) )
                    printf( "Error %u in WinHttpReadData.\n", GetLastError( ) );
                else
                    printf( "%s", pszOutBuffer );

                // Free the memory allocated to the buffer.
                delete [] pszOutBuffer;
            }
        } while( dwSize > 0 );
    }


    // Report any errors.
    if( !bResults )
        printf( "Error %d has occurred.\n", GetLastError( ) );

    // Close any open handles.
    if( hRequest ) WinHttpCloseHandle( hRequest );
    if( hConnect ) WinHttpCloseHandle( hConnect );
    if( hSession ) WinHttpCloseHandle( hSession );
}