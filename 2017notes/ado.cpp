#include<iostream>
#include<afx.h>
// no_namespace是指忽略命名空间，rename则是把ADO中的EOF重命名为adoEOF。命成什么名字无所谓，但注意声明中的名字要和代码中的名字一致
#import "C:/Program Files/Common Files/System/ado/msado15.dll" no_namespace rename("EOF","adoEOF")
using namespace std;

int main()
{
    //初始化com组件，com组件以动态链接库的形式使用
    CoInitialize(NULL);
    try
    {
        _ConnectionPtr pConn(__uuidof(Connection));
        _RecordsetPtr pRec(__uuidof(Recordset));
        _CommandPtr pCmd(__uuidof(Command));

        //和下面的方式一样
        // _ConnectionPtr pConn();
        // _RecordsetPtr pRec();
        // _CommandPtr pCmd();
        // pConn.CreateInstance("ADODB.Connection");
        // pRec.CreateInstance("ADODB.Recordset");
        // pCmd.CreateInstance("ADODB.Command");
        pConn->ConnectionString = "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=sa;Password=;Initial Catalog=Test";
        pConn->Open("","","",adConnectUnspecified); 
        CString strSQL="update employee set name='Richard' where id=1";
        // pConn->Execute(_bstr_t(strSQL),NULL,adCmdText);
        VARIANT v;
        v.vt=VT_I4;
        pConn->Execute(_bstr_t(strSQL),&v,adCmdText);
        printf("%ld\n",v.lVal);
    }
    catch(_com_error& e)
    {
        // IDispatch error #3088
        // 至少一个参数没有被指定值。
        // sql语句错误
        cerr<<e.ErrorMessage()<<endl;
        cerr<<e.Description()<<endl;
    }
    CoUninitialize();
}