#include<iostream>
using namespace std;

class C
{
	static const int a =1;  //只有静态常量整形数据成员才可以在类中初始化
public:
	int b;
	//int const b =1;
};

class CInit
{
public:
	const int b;
	CInit(int n) : b(n){}; //只有构造函数可以有成员初始化列表
};

int main()
{
	C c0,c1;
	cout<<c0.b<<endl;
	CInit c2(2);
	return 0;
}