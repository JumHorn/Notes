#pragma comment(lib,"Advapi32.lib")
#include <windows.h>
#include <wchar.h>
#include <Lmcons.h>
int wmain(void) {

	wchar_t computerName[MAX_COMPUTERNAME_LENGTH + 1];
	DWORD size = sizeof(computerName) / sizeof(computerName[0]);

	int r = GetComputerNameW(computerName, &size);

	if (r == 0) {
		wprintf(L"Failed to get computer name %ld", GetLastError());
		return 1;
	}

	wprintf(L"Computer name: %ls\n", computerName);

	wchar_t username[UNLEN + 1];
	DWORD len = sizeof(username) / sizeof(wchar_t);

	r = GetUserNameW(username, &len);

	if (r == 0) {
		wprintf(L"Failed to get username %ld", GetLastError());
		return 1;
	}

	wprintf(L"User name: %ls\n", username);

	return 0;
}