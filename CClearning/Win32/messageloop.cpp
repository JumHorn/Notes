/*
while(GetMessage(&Msg, NULL, 0, 0) > 0)
{
    TranslateMessage(&Msg);
    DispatchMessage(&Msg);
}

GetMessage是阻塞的等待系统的消息，TranslateMessage是将消息翻译成DWORD 32bit的整数信息
DispatchMessage()是将消息发送到对应的窗口，是阻塞的，它等待窗口处理完消息的返回值

PostMessage(hwnd,WM_CLOSE,0,0)

wPara --->  HIWORD   高16bit(0xFFFF0000)
	  --->  LOWORD	 低16bit(0x0000FFFF)

lPara lParam is the HWND (window handle) to the control which sent the message or NULL if the messages isn't from a control

*/