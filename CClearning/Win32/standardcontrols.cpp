/*
	One thing to remember about controls is that they are just windows.
	Like any other window they have a window procedure, a window class etc... that is registered by the system. 
	Anything you can do with a normal window you can do with a control.
*/