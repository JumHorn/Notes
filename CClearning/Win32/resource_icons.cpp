/*
#include "resource.h"

IDI_MYICON ICON "my_icon.ico"

// Icon with lowest ID value placed first to ensure application icon
// remains consistent on all systems.

identifier              type                    name of the external file which contains it(文件具体路径)

IDI_ICON6               ICON                    "res\\104A.ICO"
IDI_ICON7               ICON                    "res\\1000.ICO"
IDI_ICON9               ICON                    "res\\FORMATDO.ICO"
IDI_ICON10              ICON                    "res\\HardLine Run.ico"
IDI_ICON11              ICON                    "res\\HOSPITAL.ICO"
IDI_ICON12              ICON                    "res\\icon.ico"
IDI_ICON13              ICON                    "res\\icon1.ico"
IDI_ICON14              ICON                    "res\\MMDOC.ICO"
IDI_ICON15              ICON                    "res\\Network Neighbourhood.ico"
IDI_ICON16              ICON                    "res\\Sys.ico"
IDI_ICON17              ICON                    "res\\图标0003.ICO"
IDI_ICON18              ICON                    "res\\图标0124.ICO"
IDI_ICON19              ICON                    "res\\图标0127.ICO"
IDI_ICON20              ICON                    "res\\图标0129.ICO"
IDI_ICON21              ICON                    "res\\注销.ICO"
IDI_ICON22              ICON                    "res\\icon22.ico"


HICON hMyIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MYICON));
The first parameter of LoadIcon() and many other resource using functions is the handle to the current instance 
(which we are given in WinMain() and can also be retreived by using GetModuleHandle() as demonstrated in previous sections). 
The second is the identifier of the resource.
You're probably wondering what's up with MAKEINTRESOURCE() 
and possibly wondering why LoadIcon() takes a parameter of type LPCTSTR instead of say UINT when we're passing it an ID.
All MAKEINTRESOURCE() does is cast from an integer (what our ID is) to LPCTSTR, which LoadIcon() expects.
This brings us to the second way of identifying resources, and that's with strings.
Almost nobody does this any more, so I won't go into details, 
but basically if you don't use #define to assign an integer value to your resources then the name is interpreted as a string, and can be referenced in your program like this:

HICON hMyIcon = LoadIcon(hInstance, "MYICON");


*/