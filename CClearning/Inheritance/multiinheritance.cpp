#include<iostream>
using namespace std;

class Dog
{
public:
	virtual void bark()=0;
	int work();
};

class Cat
{
public:
	virtual void bark()=0;
};


//要实现所有纯虚函数，不然还是抽象类
class tiger:public Dog,public Cat
{
public:
	//do not write like this
	//void Dog::bark() override;
	//void Cat::bark() override;

	//父类名重复，只会覆盖
	void bark()
	{
		cout<<"redefine"<<endl;
	}
	// void Dog::bark()  //cannot define member function
	// {
	// 	cout<<"wang"<<endl;
	// }
};


int main()
{
	tiger T;
	T.bark();
}
