#include<iostream>
using namespace std;

class Dog
{
private:
    int a[10];
public:
    int & operator[](int i);
    int operator[](int i) const;
};

int& Dog::operator[](int i)
{
    cout<<"first"<<endl;
    return a[i];
}

int Dog::operator[](int i) const
{
    cout<<"last"<<endl;
    return a[i];
}

void test(const Dog& d)
{
    d[1];
}
int main()
{
    // const Dog d; //has no user-provided default constructor
    Dog d;
    const Dog* m = &d;
    (*m)[1];
    test(d);
}