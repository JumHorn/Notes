#include<iostream>
using namespace std;

//只用public继承，才能完成派生类向基类的转换
class Animal
{
	public:
		virtual void eat()
		{
			cout<<"anything"<<endl;
		}
};

class Dog : public Animal
{
	private:   //这里的eat()访问性对于父类依然是public
		void eat() //父类对象可以访问，子类对象不可访问
		{
			cout<<"meat"<<endl;
		}
};

class Cat 
{
	protected:
		void eat()
		{
			cout<<"fish"<<endl;
		}
};

class thecat : public Cat
{
public:
    thecat()
    {
        eat();
    }
};

int main()
{
	Animal *littledog = new Dog();
	littledog->eat();

	Dog *thedog = new Dog();
	//thedog->eat();   //子类对象不可访问

	thecat C;
	//C.eat();  //除了public外，都不可类外访问
}
