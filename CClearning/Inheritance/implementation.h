#ifndef _IMPLEMENTATION_H_
#define _IMPLEMENTATION_H_

class Animal
{
public:
    virtual void bark()=0;
};

//no 'void Dog::bark()' member function declared
class Dog : public Animal
{
public:
    void bark();
};

#endif