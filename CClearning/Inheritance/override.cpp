#include<iostream>
using namespace std;

class Animal
{
public:
    virtual void bark()
    {
        cout<<"wang"<<endl;
    }
};

class Dog : public Animal
{
public:
    void bark(int a)
    {
        cout<<"wa"<<endl;
    }
};
//继承中没有函数重载
//虚函数的原型只取决于函数名(与C语言函数原型一致)
int main()
{
    // Dog *d = new Dog();
    Animal *d = new Dog();
    d->bark(4);
}