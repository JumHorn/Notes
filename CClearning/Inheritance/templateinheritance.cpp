#include<iostream>
using namespace std;

class Animal
{
public:
    template<typename T>
    void print(T obj)
    {
        cout<<obj<<endl;
    }
};
//模板成员可以继承
class Dog : public Animal
{};

int main()
{
    Animal a;
    a.print("hello world");

    Dog d;
    d.print(666);
}