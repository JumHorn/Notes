#include<iostream>
using namespace std;

class Animal
{
public:
    void bark()
    {
        cout<<"wang"<<endl;
    }
};

//没有virtual下面的调用存在二义性
class Dog : virtual public Animal
{};

class Cat : virtual public Animal
{};

class Pig : public Dog, public Cat
{};

int main()
{
    Pig P;
    P.bark();
}