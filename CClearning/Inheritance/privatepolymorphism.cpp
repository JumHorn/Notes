#include<iostream>
using namespace std;

class Animal
{
public:
    virtual void bark();
};

class Dog : private Animal
{
public:     //it should be private
    void bark()
    {
        cout<<"wang"<<endl;
    }
};

int main()
{
    Dog d0;
    Dog *d1;
    //_test.obj : error LNK2001: 无法解析的外部符号 "public: virtual void __thiscall Animal::bark(void)" (?bark@Animal@@UAEXXZ)
    //d0.bark();

    //编译通过，执行错误
    //d1->bark();

    //'Animal' is an inaccessible base of 'Dog',private不允许这样转换
    Animal *a = new Dog();
    a->bark();
}

