#include<iostream>
using namespace std;


//类的默认是private，没有加修饰符的均是private
//结构体的默认是public，不需要修饰符
class Animal
{
public:
	//无参构造函数
	Animal()
	{
		height =12;
	}

	int height;
	
	int getheight()
	{
		return height;
	}
protected:
	int weight;

	int getweight()
	{
		return weight;
	}
private:
	int length;

	int getlength()
	{
		return length;
	}
};

class Dog0 : public Animal
{
public:
	Dog0()
	{
		height = 20;
	}
protected:
	int height;
	int getheight()
	{
		return height;
	}
};

class Dog : public Animal
{};

class Cat : protected Animal
{
//可以在类内部实现隐式向上转化，类外声明对象后不可以
// public:
// 	void test()
// 	{
// 		Animal *a = new Cat();
// 	}
};

class Pig : private Animal
{
public:
	void printheight()
	{
		cout<<height<<endl;
	}
};


int main()
{
	//类内部的访问修饰符，要针对类本身和类的对象去理解
	//public表示类本身和类的对象都可以访问
	//protected表示类和子类可以访问，类的对象和子类的对象都不可访问
	//private表示类本身可以访问，其他都不可以访问
	Animal A;
	cout<<A.height<<endl; //可以访问
	//A.weight;             //对象不可访问protected，但是在子类的内部可以访问
	Dog D;
	cout<<D.height<<endl;
	Cat C;
	// C.height;              //不可访问
	Dog0 D0;
	// D0.height;                //不可访问
	// Animal* a = new Dog0();
	// cout<<a->getheight()<<endl;					//public继承，protected实现，子类对象不可访问，父类对象访问父类本身

	//除了public继承，父类都不可调用子类
	//在私有继承中，在不进行显示类型转换的情况下，不能将指向派生类的引用或指针赋值给基类的引用或指针
	//在保护继承中，只有在类内部可以实现隐式向上转换
	//Animal* a = new Cat();
	//a->height;

	//除了public继承，父类都不可调用子类,但是可以使用强制类型转换
	Animal* a = (Animal*)(new Cat());
	a->height;

	//私有继承可以访问类的public成员和protected成员，但是对象不可访问
	Pig p;
	p.printheight();
}