#include<iostream>
using namespace std;

//只用public继承，才能完成派生类向基类的转换
class Animal
{
	public:
		virtual void eat()=0;
        
        void bark()
        {
            cout<<"wang"<<endl;
        }
};

//子类要实现父类的所有纯虚函数才不是抽象类
class Dog : public Animal
{
	private:   //这里的eat()访问性对于父类依然是public
		void eat() //父类对象可以访问，子类对象不可访问
		{
			cout<<"meat"<<endl;
        }
    public:
        void bark()
        {
            cout<<"wo"<<endl;
        }
};


int main()
{
    // Animal A;        //纯虚函数无法被实例化
	Animal *littledog = new Dog();
	littledog->eat();
    littledog->bark();   //调用父类方法,只有virtual方法才会先调用子类方法

    Dog *thedog = new Dog();
    thedog->bark();     //调用子类方法，相当于子类方法覆盖父类方法
	//thedog->eat();   //子类对象不可访问
}
