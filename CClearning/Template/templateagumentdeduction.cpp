#include<iostream>
using namespace std;

template<typename T>
typename T::ElementT at(T const& a,int i)
{
    return a[i];
}

void f(int* p)
{
    int x = at<int*>(p,7); //参数类型导致返回类型不匹配
}