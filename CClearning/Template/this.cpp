/*
标准C++声明:非依赖名称不会再依赖基本类中进行查找，但是会立刻查找，所以下面的例子找不到
*/
#include<iostream>
using namespace std;

template<typename T>
class Base
{
public:
    void exit();
};

template<typename T>
class Derived : Base<T>
{
public:
    void foo()
    {
        //no matching function for call to exit
        exit();  //调用玩不的exit，或者出错，应该使用this->exit();
    }
};