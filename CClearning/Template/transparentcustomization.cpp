/*
模板的局部特化
类模板可以局部特化，而函数模板只能被重载
这两种机制还是有些区别的

局部特化并不会引入一个新的模板，它只是对原来的模板进行扩展，当查找到类模板的时候，刚开始只会考虑基类模板
然而，如果在选择了基本模板之后，还发现了一个局部特化，那么将会实例化该局部特化的定义
而不再实例化基本模板的定义

相反，重载的函数模板是一个分开的模板，他们之间是完全独立的。当选择要实例化哪个模板的时候，所有的重载模板都要被考虑
然后由重载解析规则试图选择一个最佳的匹配
*/

#include<iostream>
using namespace std;

template<typename T>
void print(T t)
{
    cout<<2<<endl;
}

template<typename T> 
void print(T* t)
{
    cout<<3<<endl;
}

template<typename T>
class Animal 
{};

template<typename T>
class Animal<T*>
{};

int main()
{
    print((int*)0);
}