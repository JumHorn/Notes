#include<iostream>
using namespace std;

template<typename T1, typename T2, typename RT>
RT test(T1 a, T2 b)
{
    cout<<"RT test(T1 a, T2 b)"<<endl;
    RT rt;
    return rt;
}

//按照顺序推演
template<typename RT, typename T1, typename T2>
RT test1(T1 a, T2 b)
{
    cout<<"RT test1(T1 a, T2 b)"<<endl;
    RT rt;
    return rt;
}

template<typename T>
T test1(T a, T b)
{
    cout<<"T test1(T1 a, T2 b)"<<endl;
    T t;
    return t;
}