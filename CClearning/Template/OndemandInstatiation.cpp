#include<iostream>
using namespace std;

template<typename T>
class C;

// C<int> p;  //has incomplete type and cannot be defined
C<int> *p;    //fine definition of C<int> not needed

template<typename T>
class C
{
public:
    void f();
};

void g(C<int>& c)    // use class template declaration only
{                    
    c.f();           //use class template definition. will need definition of C::f()   
}