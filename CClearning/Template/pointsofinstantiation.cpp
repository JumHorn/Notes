#include<iostream>
using namespace std;

class MyInt
{
public:
    MyInt(int i);
};

MyInt operator - (MyInt const&);

bool operator > (MyInt const&, MyInt const&);
typedef MyInt Int;
// typedef int Int; //POI lookup not take place 

template<typename T>
void f(T i)
{
    if(i>0)
    {
        g(-i);
    }
}

void g(Int)
{
    f<Int>(42); //point of call
}

/*----------------------------------------------------------*/
template<typename T>
class S
{
public:
    T m;
};

unsigned long h()
{
    return (unsigned long)sizeof(S<int>);
}