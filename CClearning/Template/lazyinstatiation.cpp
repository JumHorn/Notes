#include<iostream>
using namespace std;

template<typename T>
class Safe
{};

template<int N>
class Danger
{
public:
    typedef char Block[N];  //would fail for N<=0
};

template<typename T,int N>
class Tricky
{
public:
    virtual ~Tricky(){}
    void no_body_here(Safe<T> = 3);   //这里不合理，编译器不会报错
    // void no_body_here(Safe<T>=3);  //不加空格编译不通过
    void inclass()
    {
        Danger<N> no_boom_yet;
    }
    // void error(){ Danger<0> boom; }
    // void unsafe(T (*p)[N]);
    T operator->();
    // virtual Safe<T> suspect();
    struct Nested
    {
        Danger<N> pfew;
    };
    union 
    {   // anonymous union
        int align;
        Safe<T> anonymous;
    };
};

int main()
{
    Tricky<int, 0> ok;
    //ok.no_body_here();//不调用不会出错
}
