#include<iostream>
using namespace std;

class Animal
{
public:
    //templates may not be 'virtual'
    template<typename T> virtual void bark(T& t);
};
