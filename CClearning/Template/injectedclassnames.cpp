#include<iostream>

int C;

class C
{
private:
    int a[2];
public:
    static int f()
    {
        return sizeof(C);
    }
};

int f()
{
    return sizeof(C);
}

int main()
{
    std::cout<< "C::f() = "<<C::f() <<","
             << " ::f() = "<<::f()<<std::endl;
}