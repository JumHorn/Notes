#include<iostream>
using namespace std;

template<typename T>
void Greet()
{
    // cout<<T<<endl;
    cout<<"hello world"<<endl;
}

int main()
{
    //couldn't deduce template parameter 'T'
    // Greet(); 
    
    Greet<int>();
}