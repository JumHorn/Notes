#include<iostream>
using namespace std;

template<typename T>
class C
{
    friend void f();
    friend void f(C<T> const&);
};

void g(C<int>* p)
{
    f();    //'f' was not declare in this scope
    f(*p);  //f(C<int> const&) is visible here
}