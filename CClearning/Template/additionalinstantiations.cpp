#include<iostream>
using namespace std;

template<typename T>
class S
{
public:
    typedef int I;
};

template<typename T>
void f()
{
    S<char>::I var1 = 41;
    typename S<T>::I var2 = 42;
}

int main()
{
    f<double>();
}