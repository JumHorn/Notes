#include<iostream>
#include<windows.h>
using namespace std;

// windows 下的调用方式
int main()
{
	typedef void (*pfunc)(void);
	HINSTANCE hDll=LoadLibrary("E:\\CClearning\\DynamicLinkLibrary\\dll.dll");
	if(!hDll)
	{
		cout<<"LoadLibrary Error: "<<GetLastError()<<endl;
		return 0;
	}
	cout<<"dll.dll的句柄地址："<< hDll << endl;

	//pfunc本身表示指针，不能再用pfunc*
	//pfunc * pf=(pfunc *)GetProcAddress(hDll,"?greet@@YAXXZ");

	pfunc pf=(pfunc)GetProcAddress(hDll,"?greet@@YAXXZ");
	if(!pf)
	{
		cout<<"GetProcAddress Error: "<<GetLastError()<<endl;
		return 0;
	}
	cout<<"dll.dll内的greet()函数的地址："<< pf <<endl;

	pf();
	//(*pf)();
	//使用完毕后，释放dll文件
	FreeLibrary(hDll);
}