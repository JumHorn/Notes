#include <winsock2.h>   
#include <stdio.h>     
#include <errno.h>
                    
#define PORT           5150    
#define MSGSIZE        1024    
                   
#pragma comment(lib, "ws2_32.lib")      
                   
int main()      
{      
    //这个结构被用来存储 被WSAStartup函数调用后返回的 Windows Sockets数据。它包含Winsock.dll执行的数据。
    WSADATA wsaData;      
    SOCKET sListen;      
    SOCKET sClient;      
    
    // struct sockaddr_in {
    //         short   sin_family;
    //         u_short sin_port;
    //         struct  in_addr sin_addr;
    //         char    sin_zero[8];
    // };

    SOCKADDR_IN server;      
    SOCKADDR_IN client;   

    char szMessage[MSGSIZE];      
    int ret;      
    int iaddrSize = sizeof(SOCKADDR_IN);      
    // 第一个参数是调用都想要使用的版本号，是一个WORD类型的变量。这个变量的高字节指定了次版本号，低字节指定了主版本号，
    //两个字节加到一起，就是你想要的Winsock库的版本号了。比如，你的代码中的版本就是2.2。
    // 第二个参数指向了一个WSADATA结构体的指针，这个结构体是你创建的，然后把这个结构体传递给WSAStartup函数，
    //它会在这个结构体中返回WinSock库的一些信息，如版本号，监听队列的大小，你能创建的最多的socket数目
    WSAStartup(0x0202, &wsaData);      
                   
    sListen = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);      
                   
    server.sin_family = AF_INET;      

    // htonl()--"Host to Network Long"
    // ntohl()--"Network to Host Long"
    // htons()--"Host to Network Short"
    // ntohs()--"Network to Host Short"

    server.sin_port = htons(PORT);      
    //INADDR_ANY任意地址，由系统定义
    server.sin_addr.s_addr = htonl(INADDR_ANY);      
    ret = bind(sListen, (struct sockaddr *) &server, sizeof(SOCKADDR_IN));      
    printf("%d:%d\n",ret,WSAGetLastError());   //测试bind返回
    
    listen(sListen, 1);      
                   
    sClient = accept(sListen, (struct sockaddr *) &client, &iaddrSize);      
    printf("Accepted client:%s:%d\n", inet_ntoa(client.sin_addr),      
            ntohs(client.sin_port));      
                    
    while(true)
    {      
        ret = recv(sClient, szMessage, MSGSIZE, 0);    
        //防止客户端断开连接 
        if(ret<=0)
        {
            printf("disconnected\n");
            break;
        } 
        szMessage[ret] = '\0';      
        printf("Received [%d bytes]: '%s'\n", ret, szMessage);    
        
        //向客户端发送消息
        int num=send(sClient, "hello", 6, 0);
        printf("%d\n",num);

    }    
    closesocket(sClient);  
    return 0;      
  }  