sock= socket(AF_INET,SOCK_STREAM,0);


struct sockaddr_in addr;      //告诉sock 应该再什么地方licence
memset(&addr,0,sizeof(addr));
addr.sin_family=AF_INET;
addr.sin_port=htons(11111);   //端口啦
addr.sin_addr.s_addr=htonl(INADDR_ANY);        //在本机的所有ip上开始监听

bind (sock,(sockaddr *)&addr,sizeof(addr));//bind....

listen(sock,5);                   //最大5个队列

SOCKET socka;                    //这个用来接受一个连接
fd_set rfd;                     // 描述符集 这个将用来测试有没有一个可用的连接
struct timeval timeout;

FD_ZERO(&rfd);                     //总是这样先清空一个描述符集

timeout.tv_sec=60;                //等下select用到这个
timeout.tv_usec=0;

u_long ul=1;

ioctlsocket(sock,FIONBIO,&ul);    //用非阻塞的连接

//现在开始用select
FD_SET(sock,&rfd);    //把sock放入要测试的描述符集 就是说把sock放入了rfd里面 这样下一步调用select对rfd进行测试的时候就会测试sock了(因为我们将sock放入的rdf) 一个描述符集可以包含多个被测试的描述符, 
if(select(sock+1,&rfd,0,0, &timeout)==0) 
{ //这个大括号接上面的,返回0那么就超过了timeout预定的时间

//处理....

}

if(FD_ISSET(sock,&rfd))
{      //有一个描述符准备好了

socka=accept(sock,0,0);     //一个用来测试读 一个用来测试写

FD_ZERO(&rfd);

FD_ZERO(&wfd);

FD_SET(socka,&rfd);//把socka放入读描述符集

FD_SET(sockb,&rfd);//把sockb放入读描述符集

FD_SET(socka,&wfd);把socka放入写描述符集

FD_SET(sockb,&wfd);把sockb放入写描述符集

if(SOCKET_ERROR!=select(0,&rfd,&wfd,0,0))      //测试这两个描述符集,永不超时 其中rfd只用来测试读 wfd只用来测试写

{      //没有错误

if(FD_ISSET(socka,&rfd))    //socka可读

{...}

if(FD_ISSET(sockb,&rfd)   //sockb可读

{...}

if(FD_ISSET(socka,&wfd) //socka 可写

{...}

if(FD_ISSET(sockb,&wfd) //sockb可写

{...}

}

