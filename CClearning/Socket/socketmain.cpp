#include<iostream>
#include<sstream>
#include"socket.h"
using namespace std;

//部分函数声明部分
void TestBase();
void TestSendFile();

int main()
{
    // TestBase();
    TestSendFile();
    return 0;
}

//不能分步执行导致卡死
void TestSendFile()
{
    cout<<"server start ..."<<endl;
    CSocket server(8490);
    server.ServerStart();

    cout<<"client start ..."<<endl;
    CSocket client("127.0.0.1",8490);
    client.ClientStart();

    cout<<"server accept connection ..."<<endl;
    server.Accept();
    cout<<server.GetIp()<<endl; 
    cout<<server.GetPort()<<endl;

    client.SendFile("E:\\a.txt","E:\\z\\a.txt");

    server.RecvFile();
	client.Close();

}

void TestBase()
{
    cout<<"server start..."<<endl;
    CSocket server(8490);
    // server.Socket();  //此处用于检查socket是否创建成功
    // server.Bind();
    // server.Listen();
    server.ServerStart();

    cout<<"client0 start..."<<endl;
    CSocket client0("127.0.0.1",8490);
    // client0.Socket();
    // client0.Connect();
    client0.ClientStart();

    // cout<<"client1 start..."<<endl;
    // CSocket client1("127.0.0.1",8490);
    // client1.Socket();
    // client1.Connect();


    cout<<"server accept connection0..."<<endl;
    server.Accept();
    cout<<server.GetIp()<<endl; 
    cout<<server.GetPort()<<endl;
    
    //服务端向客户端发送消息
    cout<<"send hello to client ..."<<endl;
    server.Send("hello client0");

    //客户端接收
    cout<<"get message ..."<<endl;
    int len;
    len=client0.Receive();
    cout<<len<<endl;
    cout<<client0.recvbuff<<endl;

    //客户端向服务端发送消息

    // cout<<"server accept connection1..."<<endl;
    // server.Accept();
    // cout<<server.GetIp()<<endl; 
    // cout<<server.GetPort()<<endl;

    //死循环不让程序停止运行
    while(true){}
}

