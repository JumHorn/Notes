#include <winsock2.h>   
#include <stdio.h>     
                    
//定义程序中使用的常量      
#define SERVER_ADDRESS "192.168.1.114" //服务器端IP地址      
#define PORT           8490         //服务器的端口号      
#define MSGSIZE        1024         //收发缓冲区的大小      
#pragma comment(lib, "ws2_32.lib")      
                   
int main()      
{      
    WSADATA wsaData;      
    //连接所用套节字      
    SOCKET sClient;      
    //保存远程服务器的地址信息      
    SOCKADDR_IN server;      
    //收发缓冲区      
    char szMessage[MSGSIZE];      
    //成功接收字节的个数      
    int ret;      
                   
    // Initialize Windows socket library      
    WSAStartup(0x0202, &wsaData);      
                   
    // 创建客户端套节字      
    sClient = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP); //AF_INET指明使用TCP/IP协议族；      
                                                         //SOCK_STREAM, IPPROTO_TCP具体指明使用TCP协议      
    // 指明远程服务器的地址信息(端口号、IP地址等)      
    memset(&server, 0, sizeof(SOCKADDR_IN)); //先将保存地址的server置为全0      
    server.sin_family = PF_INET; //声明地址格式是TCP/IP地址格式      
    server.sin_port = htons(PORT); //指明连接服务器的端口号，htons()用于    
    server.sin_addr.s_addr = inet_addr(SERVER_ADDRESS); //指明连接服务器的IP地址      
    
    int n;
    n=connect(sClient, (struct sockaddr *) &server, sizeof(SOCKADDR_IN)); //连接后可以用sClient来使用这个连接      
    printf("%d\n",n);
    
    WSACleanup();      
    return 0;      
}