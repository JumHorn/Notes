#ifndef _STATICVARIABLE_H_
#define _STATICVARIABLE_H_
//完全可以在头文件中不含这条，来起到隐藏函数的作用
void test();    //void test()' was declared 'extern' and later static
extern int global;

#endif