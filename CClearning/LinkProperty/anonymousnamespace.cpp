#include<iostream>
using namespace std;

//对于一组多个标识符函数只需要使用一个匿名空间来声明，不需要多次输入static。
namespace
{
    int m;
    void test()
    {
        cout<<"hello world"<<endl;
    }
}
//using namespace __UNIQUE_NAME_; //相当于在这里使用上面这个命名空间

int main()
{
    cout<<m<<endl;
    test();
}