#ifndef _REDEFINE0_H_
#define _REDEFINE0_H_

#include<iostream>
using namespace std;

//变量的定义
// int i;         //错误  multiple definition of i
// int j=0;       //错误  multiple definition of j
// extern int k=0;//错误  initialised and declare

// //变量的声明
extern int k;  //通过
// extern const int l =5;   //错误  multiple definition of l

// //函数的定义
// void func()    //错误  multiple definition of func()
// {}

// //函数的声明
void fun();    //正确

// //类的声明
// class Dog      //正确
// {
// public:
//     void bark();
// };

// //类的定义
class Dog   //编译通过，最终输出和main中inlude顺序有关，先定义为准
{
public:
    void bark(){cout<<"wa"<<endl;}
};

#endif