#include<iostream>
#include"scope1.h"
using namespace std;
using namespace myarea1; //using编译指令，导入所有命名空间
//命名空间的作用是全局的，不能跨越文件级别
namespace myarea
{
    namespace internal
    {
        void test()
        {
            myarea1::Greet();  //Greet is not a member of myarea
        }
    }
}

namespace scope = myarea::internal; //简化命名空间

int main()
{
    using scope::test;  //using声明，导入指定名称
    test();
}