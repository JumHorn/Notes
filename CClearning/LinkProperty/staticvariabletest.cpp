//若是包含cpp文件，则可以认为是同一个文件
// #include"staticvariable.cpp"
#include"staticvariable.h"
#include<iostream>
using namespace std;
int main()
{
    // test();     //这条gcc编译不通过 void test()' was declared 'extern' and later static，vc编译通过，链接失败
    // global;     //这条语句可以编译通过，但是没有意义
    // cout<<global<<endl;   //下面这条语句不能编译通过
}