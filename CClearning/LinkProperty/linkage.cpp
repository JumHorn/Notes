#include<iostream>
using namespace std;

//这种变量在头文件中声明，可以用于其他文件
// int global;    //static duration ,external linkage
//这种变量无法早头文件中声明
static int global = 5;   //static duration ,intern

int internal()
{
    static int global =7;  //static duration ,no linkage
    cout<<global<<endl;
    cout<<::global<<endl;
}

int main()
{
    internal();
}