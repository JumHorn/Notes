#include"staticvariable.h"
#include<iostream>
using namespace std;

//文件内作用
static void test()
{
    cout<<"hello world"<<endl;
}

// static int global =5;  //'global' was declared 'extern' and later 'static conflict
int global = 5;