#include<iostream>
#include<windows.h>
using namespace std;

#define MAXTHREAD 100
static volatile int number=0;
// static int number = 0;
// 原子操作不需要加同步，++操作需要同步
// int number = 0;

CRITICAL_SECTION ThreadParameter;

DWORD WINAPI WinThread( LPVOID lParam )
{
    // Sleep(1000);  //模拟其他操作需要的时间
    int *p=(int*)lParam;
    EnterCriticalSection(&ThreadParameter);
    (*p)++;
    LeaveCriticalSection(&ThreadParameter);
    cout<<*p<<endl;
    // cout<<++number<<endl;
    return 0;
}

int main()
{
    int num=0;
    HANDLE hThread[MAXTHREAD];  
	InitializeCriticalSection(&ThreadParameter);
	for(int i=0;i<MAXTHREAD;i++)
	{
        // HANDLE handle = CreateThread(NULL,0,WinThread,&number,0,NULL);
        hThread[i] = CreateThread(NULL,0,WinThread,&num,0,NULL);   
        if(hThread[i]==NULL)
        {
            cout<<"failed"<<endl;
        }
    }
    Sleep(1000);
    WaitForMultipleObjects(MAXTHREAD, hThread, TRUE, INFINITE); 
	DeleteCriticalSection(&ThreadParameter);
    cout<<"the last num:"<<num<<endl;
    
    return 0;
}
