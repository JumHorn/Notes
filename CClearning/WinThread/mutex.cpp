// 互斥量Mutex：
//互斥量和关键段的区别互斥量可以设置等待时间

// Usage
// WaitForSingleObject(hMutex,…);  
// ...//do something  
// ReleaseMutex(hMutex);  

#include <stdio.h>
#include <process.h>
#include <windows.h>

long global;
unsigned int __stdcall ThreadFunction(void *handle);
const int THREAD_NUM = 10;
// 互斥量
HANDLE hmutex; 

int main()
{
    // 初始化互斥量,第二个参数为TRUE表示互斥量为创建进程所有
    hmutex = CreateMutex(NULL, FALSE, NULL);

    HANDLE handle[THREAD_NUM];
    global = 0;
    int i = 0;
    while (i < THREAD_NUM) 
    {
        handle[i] = (HANDLE)_beginthreadex(NULL, 0, ThreadFunction, &i, 0, NULL);
        i++;
    }
    WaitForMultipleObjects(THREAD_NUM, handle, TRUE, INFINITE);
    // 销毁互斥量
    CloseHandle(hmutex);
    return 0;
}

unsigned int __stdcall ThreadFunction(void *handle)
{
    WaitForSingleObject(hmutex, INFINITE);  // 等待互斥量被触发
    global++;
    Sleep(0);
    printf("global variable is %d\n", global);
    ReleaseMutex(hmutex);   // 触发互斥量
    return 0;
}