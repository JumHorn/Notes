// 1、线程的挂起
// DWORD SuspendThread(HANDLE hThread)
// 返回值：成功则返回线程被挂起的次数；失败则返回0XFFFFFFFF。
// 2、线程的恢复
// DWORD ResumeThread(HANDLE hTread)
// 返回值：成功则返回线程被挂起的次数；失败则返回0XFFFFFFFF。
// 4、线程的结束
// 在线程内调用AfxEndThread将会直接结束线程，此时线程的一切资源都会被回收.
// 注意在线程中使用了CString类，则不能用AfxEndThread来进行结束线程，会有内存泄漏，只有当程序结束时，会在输出窗口有提示多少byte泄漏了。
// 因为Cstring的回收有其自己的机制。建议在AfxEndThread之前先进行return

#include<iostream>
//#include<windows.h>  //MFC apps must not #include <windows.h>
#include <afxwin.h>  //win32程序调用AfxBeginThread
using namespace std;
UINT WinThread( LPVOID lParam ) //windows AfxBeginThread函数标准格式
{
    for(int i=0;i<5;i++)
    {
        cout<<"hello thread"<<endl;
    }
    return 0;
}

int main()
{
    CWinThread* m_pthread=AfxBeginThread(WinThread,NULL);
    if(!m_pthread)
    {
        cout<<"线程创建失败"<<endl;
    }
    for(int i=0;i<5;i++)
    {
        cout<<"hello main"<<endl;
    }
    //这里主线程比子线程提前退出，导致程序提前结束，子线程卡死
    //CreateThread也发生这种现象
    char in;
    cin>>in;
    while(in!='q')cin>>in;
    return 0;
}