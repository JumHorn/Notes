#include <stdio.h>
#include <process.h>
#include <windows.h>

// Usage
// EnterCriticalSection(CRITICAL_SECTION)
// ...//do some thing
// LeaveCriticalSection(CRITICAL_SECTION)

long global;
unsigned int __stdcall ThreadFunction(void *handle);
const int THREAD_NUM = 10;  //if the number is more than 100,there will be a question
// definition of critical section
CRITICAL_SECTION g_csThreadCode;

int main()
{
    // initialize critical section
    InitializeCriticalSection(&g_csThreadCode);

    HANDLE handle[THREAD_NUM];
    global = 0;
    int i = 0;
    while (i < THREAD_NUM) 
    {
        handle[i] = (HANDLE)_beginthreadex(NULL, 0, ThreadFunction, &i, 0, NULL);
        ++i;  
    }
    WaitForMultipleObjects(THREAD_NUM, handle, TRUE, INFINITE); //wait for all threads to finish the task
    DeleteCriticalSection(&g_csThreadCode);

    return 0;
}

unsigned int __stdcall ThreadFunction(void *handle)
{
    EnterCriticalSection(&g_csThreadCode);  // enter critical section
    global++;
    Sleep(0);                               //重新发起一次CPU竞争
    printf("global variable is %d\n",global);
    LeaveCriticalSection(&g_csThreadCode);
    return 0;
}