// Usage
// SetEvent(hEvent);
// WaitForSingleObject(hEvent,…);
// 等待其他进程的setevent，然后执行操作

#include <stdio.h>  
#include <process.h>  
#include <windows.h>  
long global;  
unsigned int __stdcall ThreadFunction(void *handle);  
const int THREAD_NUM = 10;  
//事件
HANDLE hevent;
int main()  
{  
    //初始化事件初
    hevent = CreateEvent(NULL, FALSE, FALSE, NULL);
    HANDLE  handle[THREAD_NUM];   
    global = 0;  
    int i = 0;  
    SetEvent(hevent);  //如果这一行注释，则事件没有触发，所有线程等待
    while (i < THREAD_NUM)   
    {  
        handle[i] = (HANDLE)_beginthreadex(NULL, 0, ThreadFunction, &i, 0, NULL);  
        i++;  
    }  
    WaitForMultipleObjects(THREAD_NUM, handle, TRUE, INFINITE);  
    //销毁事件
    CloseHandle(hevent);  
    return 0;  
}  
unsigned int __stdcall ThreadFunction(void *handle)  
{
    WaitForSingleObject(hevent,INFINITE);
    global++;  
    Sleep(0);
    printf("global variable is %d\n",global);   
    SetEvent(hevent); //触发事件 
    return 0;  
}  
