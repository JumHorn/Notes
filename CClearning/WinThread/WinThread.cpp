#include<iostream>
#include<windows.h>
using namespace std;

DWORD WINAPI WinThread( LPVOID lParam ) //windows thread函数标准格式
{
    for(int i=0;i<5;i++)
    {
        //cout<<*(string *)lParam<<endl;
        cout<<"hello thread"<<endl;
    }
    return 0;
}

int main()
{
    //char *p="hello word";
    //string str="hello thread";
    //CreateThread(NULL,0,WinThread,(void *)&str,0,NULL);
    HANDLE handle=CreateThread(NULL,0,WinThread,NULL,0,NULL);
    
    for(int i=0;i<5;i++)
    {
        cout<<"hello main"<<endl;
    }
    WaitForSingleObject(handle, INFINITE);  //等待线程结束
    return 0;
}