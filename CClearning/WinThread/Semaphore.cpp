// WaitForSingleObject(hsemaphore,INFINITE);
// ...//do something
// ReleaseSemaphore(hsemaphore, 1, NULL);

#include <stdio.h>
#include <process.h>
#include <windows.h>

long global;
unsigned int __stdcall ThreadFunction(void *handle);
const int THREAD_NUM = 10;
// 信号量
HANDLE hsemaphore;

int main()
{
    // 初始化信号量和关键段
    hsemaphore = CreateSemaphore(NULL, 0, 1, NULL); // 当前0个资源，最大允许1个同时访问
    ReleaseSemaphore(hsemaphore, 1, NULL);  // 信号量++,多出一个资源，如果注释掉，所有线程等待
    HANDLE handle[THREAD_NUM];
    global = 0;
    int i = 0;
    while (i < THREAD_NUM) 
    {
        handle[i] = (HANDLE)_beginthreadex(NULL, 0, ThreadFunction, &i, 0, NULL);
        ++i;
    }
    WaitForMultipleObjects(THREAD_NUM, handle, TRUE, INFINITE);

    // 销毁信号量
    CloseHandle(hsemaphore);
    return 0;
}

unsigned int __stdcall ThreadFunction(void *handle)
{
    WaitForSingleObject(hsemaphore,INFINITE);
    global++;
    Sleep(0);
    printf("global variable is %d\n",global);
    ReleaseSemaphore(hsemaphore, 1, NULL);  // 信号量++
    return 0;
}