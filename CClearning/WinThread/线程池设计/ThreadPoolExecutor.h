#ifndef _THREADPOOLEXECUTOR_H_
#define _THREADPOOLEXECUTOR_H_

#include "Thread.h"
#include<set>
#include<list>
#include<window.h>

class CThreadPoolExecutor
{
public:
	CThreadPoolExecutor();
	~CThreadPoolExecutor();

	bool Init(unsigned int minThreads, unsigned int maxThreads, unsigned int maxPendingTasks);

	bool Execute(Runnable *pRunnable);

	void Terminate();

	unsigned int GetThreadPoolSize();

private:
	Runnable * GetTask();
	static unsigned int WINAPI StaticThreadFunc(void * arg);

private:
	class CWorker : public CThread
	{
	public:
		CWorker(CThreadPoolExecutor * m_pThreadPool,Runnable * pFirstTask=NULL);
		~CWorker();
		void Run();

	private:
		CThreadPoolExecutor * m_pThreadPool;
		Runnable * m_pFirstTask;
		volatile bool m_bRun;
	};

	typedef std::set<CWorker *> ThreadPool;
	typedef std::list<Runnable *> Tasks;
	typedef Tasks::iterator TaskItr;
	typedef ThreadPool::iterator ThreadPoolItr;

	ThreadPool m_ThreadPool;
	ThreadPool m_TrashThread;
	Tasks m_Tasks;

	CRITICAL_SECTION m_csTasksLock;  //在windows.h头文件中
	CRITICAL_SECTION m_csThreadPoolLock;

	volatile bool m_bRun;
	volatile bool m_bEnableInsertTask;
	volatile unsigned int m_minThreads;
	volatile unsigned int m_maxThreads;
	volatile unsigned int m_maxPendingTasks;
};

#endif