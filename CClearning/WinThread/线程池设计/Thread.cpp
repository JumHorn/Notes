#include "Thread.h"

CThread::CThread(void):m_pRunable(NULL),m_bRun(false)
{}

CThread::~CThread(void)
{}

CThread::CThread(Runnable * pRunable) : m_ThreadName(""),m_pRunnable(pRunnable),m_bRun(false)
{}

CThread::CThread(const char * ThreadName,Runnable * pRunnable) : m_ThreadName(ThreadName),m_pRunnable(pRunnable),m_bRun(false)
{}

CThread::CThread(std::string ThreadName,Runnable * pRunnable) : m_ThreadName(ThreadName),m_pRunnable(pRunnable),m_bRun(false)
{}