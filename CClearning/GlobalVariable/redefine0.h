#ifndef _REDEFINE0_H_
#define _REDEFINE0_H_

#include<iostream>
using namespace std;

//变量声明和定义
int i;         //错误
int j=0;       //错误
extern int k;  //正确
extern int k=0;//错误

//函数声明和定义
void func()    //错误
{}

void fun();    //正确

//类声明和定义
class Dog      //正确
{
public:
    void bark();
};

class Dog   //编译通过，最终输出和main中inlude顺序有关，先定义为准
{
public:
    void bark(){cout<<"wang"<<endl;}
};

#endif