#include<iostream>
using namespace std;

struct Animal
{
    //这里所有默认public
    int weight;
    void bark()
    {
        cout<<"wa"<<endl;
    }
};

//这里默认public，所以可以省略
struct Dog : Animal
{
};

int main()
{
    Dog d;
    d.bark();
}