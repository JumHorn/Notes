#include<iostream>
using namespace std;

template<typename T>
struct Animal
{
    //这里所有默认public
    T weight;
    void bark()
    {
        cout<<weight<<endl;
    }
};

//这里默认public，所以可以省略
struct Dog : Animal<int>
{
    //TODO ...模板继承问题
};

int main()
{
    Dog d;
    d.bark();
}