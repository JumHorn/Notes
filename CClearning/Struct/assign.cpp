#include<iostream>
using namespace std;

struct Animal
{
    int weight;
    char code;
    virtual void bark()
    {
        cout<<"wa"<<endl;
    }
};

//正常情况情况下可以这样赋值，但是当函数是virtual的时候会失败
//少了虚函数地址
int main()
{
    //could not convert '{1023, 'f'}' from '<brace-enclosed initializer list>' to 'Animal'
    Animal a = {1023,'f'};
    cout<<a.code<<endl;
}