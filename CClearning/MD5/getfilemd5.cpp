/***************************************************************

		[Jum] created on 2017.9.21
		本文件包含实现获取文件md5的方法
		

****************************************************************/
#include<iostream>
#include<fstream>
#include"md5.h"
using namespace std;

int GetMD5(const char* filepath,char* md5)
{
	ifstream fin(filepath,ios::binary);
	if(!fin)
	{
		return -1;
	}

	MD5_CTX ctx;
	unsigned char buff[1024];
	int nbyte;

	MD5Init(&ctx);
	do
	{
		fin.read((char*)buff,1024);
		nbyte = fin.gcount();
		MD5Update(&ctx, buff, nbyte);
	}while(nbyte==1024);

	MD5Final(&ctx);

	for (int i = 0; i < 16; i++)
	{
		sprintf(&md5[i * 2], "%02x", ctx.digest[i]);
	}
	return 1;
}
